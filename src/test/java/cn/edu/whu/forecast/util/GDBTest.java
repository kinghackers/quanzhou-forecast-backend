//package cn.edu.whu.forecast.util;
//
//import org.gdal.gdal.gdal;
//import org.gdal.ogr.*;
//
//import java.sql.Date;
//import java.sql.Time;
//import java.sql.Timestamp;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.time.LocalTime;
//
///**
// * gdal读取gdb示例
// * @author liuyu
// * @date 2020/11/5
// */
//public class GDBTest {
//    static {
//        gdal.AllRegister();
//    }
//
//    public static void main(String[] args) {
//        Driver driver = ogr.GetDriverByName("OpenFileGDB");
//        DataSource dataSource = driver.Open("F:\\山洪预报模型\\天气雷达\\example\\GRAPES\\20200506\\Z_NAFP_C_BABJ_20200506000000_P_NWPC-GRAPES-3KM-CN-00100.gdb2", 0);
//        for (int i = 0; i < dataSource.GetLayerCount(); i++) {
//            //获取图层
//            Layer layer = dataSource.GetLayer(i);
//            System.out.println(i + "\t" + layer.GetName());
//            do {//获取图层下的要素
//                Feature feature = layer.GetNextFeature();
//                if (null == feature) {
//                    break;
//                }
//                //获取样式，这里是空，待确定
//                System.out.println(feature.GetStyleString());
//                //获取geometry
//                System.out.println(feature.GetGeometryRef().ExportToWkt());//也可以ExportToWkb() gml等等
//                //获取属性
//                for (int p = 0; p < feature.GetFieldCount(); p++) {
//                    System.out.println(feature.GetFieldDefnRef(p).GetName()
//                            + "\t" +
//                            getProperty(feature, p));
//                }
//                System.out.println("---------");
//            } while (true);
//        }
//    }
//
//    private static Object getProperty(Feature feature, int index) {
//        int type = feature.GetFieldType(index);
//        PropertyGetter propertyGetter;
//        if (type < 0 || type >= propertyGetters.length) {
//            propertyGetter = stringPropertyGetter;
//        } else {
//            propertyGetter = propertyGetters[type];
//        }
//        try {
//            return propertyGetter.get(feature, index);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * 属性获取器
//     */
//    @FunctionalInterface
//    private interface PropertyGetter {
//        Object get(Feature feature, int index);
//    }
//
//    private static final PropertyGetter stringPropertyGetter = (feature, index) -> feature.GetFieldAsString(index);
//
//    /**
//     * feature.GetFieldType(index)得到一个属性类型的int值,该值对应具体类型
//     */
//    private static final PropertyGetter[] propertyGetters = new PropertyGetter[]{
//            (feature, index) -> feature.GetFieldAsInteger(index),//0	Integer
//            (feature, index) -> feature.GetFieldAsIntegerList(index),//1	IntegerList
//            (feature, index) -> feature.GetFieldAsDouble(index),//2	Real
//            (feature, index) -> feature.GetFieldAsDoubleList(index),//3	RealList
//            stringPropertyGetter,//4	String
//            (feature, index) -> feature.GetFieldAsStringList(index),//5	StringList
//            stringPropertyGetter,//6	(unknown)
//            stringPropertyGetter,//7	(unknown)
//            (feature, index) -> feature.GetFieldAsBinary(index),//8	Binary
//            (feature, index) -> {
//                int[] pnYear = new int[1];
//                int[] pnMonth = new int[1];
//                int[] pnDay = new int[1];
//                int[] pnHour = new int[1];
//                int[] pnMinute = new int[1];
//                float[] pfSecond = new float[1];
//                int[] pnTZFlag = new int[1];
//                feature.GetFieldAsDateTime(index, pnYear, pnMonth, pnDay, pnHour, pnMinute, pfSecond, pnTZFlag);
//                Date date = Date.valueOf(LocalDate.of(pnYear[0], pnMonth[0], pnDay[0]));
//                return date;
//            },//9	Date
//            (feature, index) -> {
//                int[] pnYear = new int[1];
//                int[] pnMonth = new int[1];
//                int[] pnDay = new int[1];
//                int[] pnHour = new int[1];
//                int[] pnMinute = new int[1];
//                float[] pfSecond = new float[1];
//                int[] pnTZFlag = new int[1];
//                feature.GetFieldAsDateTime(index, pnYear, pnMonth, pnDay, pnHour, pnMinute, pfSecond, pnTZFlag);
//                float fSecond = pfSecond[0];
//                int s = (int) fSecond;
//                int ns = (int) (1000000000 * fSecond - s);
//                Time time = Time.valueOf(LocalTime.of(pnHour[0], pnMinute[0], s, ns));
//                return time;
//            },// 10	Time
//            (feature, index) -> {
//                int[] pnYear = new int[1];
//                int[] pnMonth = new int[1];
//                int[] pnDay = new int[1];
//                int[] pnHour = new int[1];
//                int[] pnMinute = new int[1];
//                float[] pfSecond = new float[1];
//                int[] pnTZFlag = new int[1];
//                feature.GetFieldAsDateTime(index, pnYear, pnMonth, pnDay, pnHour, pnMinute, pfSecond, pnTZFlag);
//                float fSecond = pfSecond[0];
//                int s = (int) fSecond;
//                int ns = (int) (1000000000 * fSecond - s);
//                LocalDateTime localDateTime = LocalDateTime.of(
//                        LocalDate.of(pnYear[0], pnMonth[0], pnDay[0]),
//                        LocalTime.of(pnHour[0], pnMinute[0], s, ns)
//                );
//                Timestamp timestamp = Timestamp.valueOf(localDateTime);
//                return timestamp;
//            },//11	DateTime
//            (feature, index) -> feature.GetFieldAsInteger64(index),//12	Integer64
//            (feature, index) -> feature.GetFieldAsIntegerList(index),//13 Integer64List
//            //>=14	(unknown)
//    };
//
//}
