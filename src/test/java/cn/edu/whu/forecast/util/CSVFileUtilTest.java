package cn.edu.whu.forecast.util;

import cn.edu.whu.forecast.service.impl.RainfallServiceImpl;
import cn.edu.whu.forecast.vo.response.RainfallVo;
import org.assertj.core.util.Arrays;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
@SpringBootTest
public class CSVFileUtilTest {

    @Autowired
    RainfallServiceImpl rainfallService;

    /**
     * 创建CSV文件
     */
    @Test
    public void createCSV() {

        List<List<Object>> heads = new ArrayList<>();
        String[] head1 = new String[]{"TIME","A","B","C","D","E","F","G","H","J","EE"};
        heads.add(Arrays.asList(head1));
        List<List<Object>> dataList = new ArrayList<>();
        List<Object> tmp;
        for(int i=0; i<100; i++){
             tmp = new ArrayList<>();
            tmp.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            for (int j = 0; j < 9; j++) {
                tmp.add(i);
            }
            tmp.add(0.625);
            dataList.add(tmp);
        }
        String fileName = "testCSV.csv";
        String filePath = "C:\\Users\\Administrator\\Desktop\\";

        CSVFileUtil.createCSV(heads,dataList,fileName,filePath,true);
    }

    @Test
    public void createCSV1(){
        List<List<Object>> heads = new ArrayList<>();
        Object[] head1 = new Object[]{"LGTD",118.1,118.216666,118.17017,118.15127,118.216666,118.266666,118.2975};
        Object[] head2 = new Object[]{"LTTD",25.433333,25.433333,25.3825,25.31467,25.35,25.4,25.3167};
        Object[] head3 = new Object[]{"STNM","锦溪","嵩山","蓬壶","达中","大卿","天马","永春"};
        heads.add(Arrays.asList(head1));
        heads.add(Arrays.asList(head2));
        heads.add(Arrays.asList(head3));
        List<List<Object>> dataList = new ArrayList<>();
        List<Object> tmp = new ArrayList<>();
        List<RainfallVo> rainfallByTime = rainfallService.getRainfallByTime(new Date(2022, Calendar.JULY, 5, 0, 0, 0));
        tmp.add(rainfallByTime.get(0).getTime());
        for (RainfallVo rainfallVo:rainfallByTime){
            tmp.add(rainfallVo.getRainfall());
        }

//        for(int i=0; i<100; i++){
//            tmp = new ArrayList<>();
//            tmp.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//            for (int j = 0; j < 9; j++) {
//                tmp.add(i);
//            }
//            tmp.add(0.625);
//            dataList.add(tmp);
//        }
        String fileName = "testCSV.csv";
        String filePath = "C:\\Users\\Administrator\\Desktop\\";

        CSVFileUtil.createCSV(heads,dataList,fileName,filePath,true);

    }

}