package cn.edu.whu.forecast.util;

import cn.edu.whu.forecast.schudule.jobs.HelloJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class HelloJobTest {
    public static void main(String[] args) throws SchedulerException {
        //获取调度器
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        //定义一个任务调度实例
        JobDetail job = JobBuilder.newJob(HelloJob.class)
                .withIdentity("job1", "group1")
                .build();

        //定义触发器，马上执行，每5秒执行一次
//        SimpleTrigger trigger = TriggerBuilder.newTrigger()
//                .withIdentity("trigger1", "group1")
//                .startNow()
//                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
//                        .repeatForever()
//                        .withIntervalInSeconds(5))
//                .build();

        CronTrigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("trigger1", "group1")
                .startNow()
                .withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * 22 6 ?"))
                .build();
        //使用触发器调度任务的执行
        scheduler.scheduleJob(job,trigger);


        //开启
        scheduler.start();

    }
}
