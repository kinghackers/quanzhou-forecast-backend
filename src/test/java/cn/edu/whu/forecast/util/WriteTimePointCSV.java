package cn.edu.whu.forecast.util;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WriteTimePointCSV {
    @Test
    public void test(){
        List<List<Object>> heads = new ArrayList<>();
        List<Object> tmpHead = new ArrayList<>();
        tmpHead.add("point");
        tmpHead.add("time");
        heads.add(tmpHead);
        List<List<Object>> dataList=  new ArrayList<>();
        List<Object> data = new ArrayList<>();
        data.add("point");
        data.add("time");
        dataList.add(data);
        List<Object> data1 = new ArrayList<>();
        List<Object> data2 = new ArrayList<>();
        data1.add("pred_time");
        data1.add(new SimpleDateFormat("yyyy/M/dd HH:mm").format(new Date()));
        dataList.add(data1);
        data2.add("pred_p_time");
        data2.add(new SimpleDateFormat("yyyy/M/dd HH:mm").format(new Date().getTime()+1000*60*60*24L));
        dataList.add(data2);

        String fileName = "timepoint.csv";
        String filePath = "C:\\Users\\Administrator\\Desktop\\";

        CSVFileUtil.createCSV(heads,dataList,fileName,filePath,false);
    }
}
