package cn.edu.whu.forecast.util;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WriteWaterLevelCSV {
    @Test
    public void test(){
        List<List<Object>> heads = new ArrayList<>();
        List<Object> tmpHead = new ArrayList<>();
        tmpHead.add("TIME");
        tmpHead.add("YC_Z");
        heads.add(tmpHead);
        List<List<Object>> dataList=  new ArrayList<>();
        List<Object> data = new ArrayList<>();
        data.add("TIME");
        data.add("YC_Z");
        dataList.add(data);
        List<Object> data1 = new ArrayList<>();
        List<Object> data2 = new ArrayList<>();
        data1.add(new SimpleDateFormat("yyyy/M/dd HH:mm").format(new Date()));
        data1.add(112.345);
        dataList.add(data1);
        data2.add(new SimpleDateFormat("yyyy/M/dd HH:mm").format(new Date().getTime()+1000*60*60*24L));
        data2.add(123.444);
        dataList.add(data2);

        String fileName = "永春水位.csv";
        String filePath = "C:\\Users\\Administrator\\Desktop\\";

        CSVFileUtil.createCSV(heads,dataList,fileName,filePath,false);
    }
}
