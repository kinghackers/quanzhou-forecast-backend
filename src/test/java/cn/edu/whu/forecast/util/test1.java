//package cn.edu.whu.forecast.util;
//
//import cn.edu.whu.forecast.vo.response.RainfallPerHourVo;
//import org.apache.poi.ss.usermodel.RichTextString;
//import org.apache.poi.ss.usermodel.WorkbookFactory;
//import org.apache.poi.xssf.usermodel.XSSFCell;
//import org.apache.poi.xssf.usermodel.XSSFRow;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.junit.Test;
//
//import java.io.*;
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//public class test1 {
//    @Test
//    public void test() throws IOException {
//        String filepath = "C:\\Users\\Administrator\\Desktop\\";
//        String filename = "testXlsx.xlsx";
//        File file = new File(filepath+filename);
//        File parent = file.getParentFile();
//        if (parent != null && !parent.exists()) {
//            parent.mkdirs();
//        }
//        file.createNewFile();
//        XSSFWorkbook wb =null;
//        XSSFSheet sheet = null;
//        InputStream input = null;
//        FileOutputStream output=null;
//        try {
//            wb = new XSSFWorkbook();
//            input = new FileInputStream(file);
//            wb = (XSSFWorkbook) WorkbookFactory.create(input);
//            if(wb != null){
//                //获取文件的指定工作表
//                sheet =wb.getSheet("sheet1");
//                output = new FileOutputStream(filepath+"/"+filename,false);
//                //获取最大行数
//                //int rownum = sheet.getPhysicalNumberOfRows();
//                int index = sheet.getLastRowNum()+1;
//                String cmdStr = "";
//                List<RainfallPerHourVo> list = new ArrayList<>();
//                list.add(new RainfallPerHourVo("1111",new Date(),new BigDecimal("0.1")));
//                list.add(new RainfallPerHourVo("1112",new Date(),new BigDecimal("0.1")));
//                list.add(new RainfallPerHourVo("1113",new Date(),new BigDecimal("0.1")));
//                list.add(new RainfallPerHourVo("1114",new Date(),new BigDecimal("0.1")));
//                list.add(new RainfallPerHourVo("1115",new Date(),new BigDecimal("0.1")));
//                if(list.size() > 0){
//                    for(int i = 0; i < list.size(); i++){
//                        cmdStr = "1231231231232";
//                        //插入excel
//                        XSSFRow row = null;
//                        row = sheet.createRow(index+i);
//                        XSSFCell cell_flow = row.createCell(0);
//                        XSSFCell cell_job = row.createCell(2);
//                        XSSFCell cell_cmd = row.createCell(6);
//                        cell_flow.setCellValue(list.get(i).getStationName());
//                        cell_job.setCellValue(list.get(i).getTime());
//                        cell_cmd.setCellValue((RichTextString) list.get(i).getRainfall());
//                    }
//                }
//                output.flush();
//                wb.write(output);
//                input.close();
//                output.close();
//            }
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//    }
//}
