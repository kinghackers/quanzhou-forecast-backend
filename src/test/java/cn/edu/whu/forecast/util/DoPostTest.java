package cn.edu.whu.forecast.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallAllForecast;
import cn.edu.whu.forecast.schudule.jobs.CrawlerJob;
import cn.edu.whu.forecast.service.impl.AvgRainfallAllServiceImpl;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;

import javax.annotation.Resource;

public class DoPostTest {

    @Test
    public void test(){
        AvgRainfallAllServiceImpl avgRainfallAllServiceImpl = (AvgRainfallAllServiceImpl) ApplicationContextUtil.getBean("AvgRainfallAllServiceImpl");


        String s = HttpClientUtil.doGet("http://124.220.210.6:8082/radar");
        JSONObject object = JSONObject.parseObject(s);
        JSONArray datas = object.getJSONArray("data");
        for(int i=0; i<datas.size(); i++) {
            JSONArray data = datas.getJSONArray(i);
            AvgRainfallAllForecast avgRainfallAllForecast = new AvgRainfallAllForecast();
            avgRainfallAllForecast.setTime(LocalDateTime.parse(data.getString(0).replaceAll("T", " "), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            avgRainfallAllForecast.setRainfallA(data.getBigDecimal(1));
            avgRainfallAllForecast.setRainfallB(data.getBigDecimal(2));
            avgRainfallAllForecast.setRainfallC(data.getBigDecimal(3));
            avgRainfallAllForecast.setRainfallD(data.getBigDecimal(4));
            avgRainfallAllForecast.setRainfallE(data.getBigDecimal(5));
            avgRainfallAllForecast.setRainfallF(data.getBigDecimal(6));
            avgRainfallAllForecast.setRainfallG(data.getBigDecimal(7));
            avgRainfallAllForecast.setRainfallH(data.getBigDecimal(8));
            avgRainfallAllForecast.setRainfallI(data.getBigDecimal(9));
            avgRainfallAllForecast.setRainfallJ(data.getBigDecimal(10));
            avgRainfallAllForecast.setRainfallYc(data.getBigDecimal(11));
            avgRainfallAllForecast.setEe(data.getBigDecimal(12));


            avgRainfallAllServiceImpl.addAvgRainfallAllForecast(avgRainfallAllForecast);
        }
    }

    @Test
    public void test1(){

    }

}


//        CriticalLevelVo criticalLevelVo = new CriticalLevelVo();
//
//        criticalLevelVo.setWaterLevel(criticalLevel.get(0).toString());
//        criticalLevelVo.setForecastResult(criticalLevel.get(1).toString());
//
//        System.out.println(criticalLevelVo);
        //System.out.println(criticalLevel);

//        String s = HttpClientUtil.doGet("http://124.220.210.6:8082/xajPredict");
//        JSONObject object = JSONObject.parseObject(s);
//        JSONArray criticalLevels = object.getJSONArray("runoffSim");
//        JSONArray criticalLevel = criticalLevels.getJSONArray(0);
//        System.out.println();


//    @Test
//    public void test1() throws IOException {
//        Map<String, Object> map = new HashMap<>();
//        map.put("criticalRainfall",new BigDecimal("143.1"));
//        map.put("warning", 1);
//        String s = HttpClientUtil.doPost("http://localhost:8888/forecast/addWarningRecord", map);
//        System.out.println(s);
//    }

