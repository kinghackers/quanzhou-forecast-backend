package cn.edu.whu.forecast.util;

import org.junit.Test;
import springfox.documentation.spring.web.json.Json;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestCmdCommander {
    @Test
    public void test() throws IOException, InterruptedException {
        // Windows下
        Process p = Runtime.getRuntime().exec(new String[]{"cmd", "/c",});
        p.waitFor();
        System.out.println("复制完成");
    }

    @Test
    public void test1(){
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(1);
        while (list.size()<10){
            list.add(1);
        }

        System.out.println(list.size());
    }

    @Test
    public void test2(){
        Date date = new Date();
        date.setSeconds(0);
        date.setMinutes(0);
        System.out.println((new SimpleDateFormat("HH:mm").format(date.getTime()-1000*60*60*9)));
    }

    @Test
    public void test3(){
        String s = "2022/08/04 10:00:00";
        System.out.println(s.substring(0,13));
        //BigDecimal bigDecimal = new BigDecimal("2.56858634988219e-03");
        //System.out.println(bigDecimal.setScale(2,BigDecimal.ROUND_HALF_UP));
    }
    @Test
    public void test4(){
        //JsonFileUtil.saveDataToFile(new SimpleDateFormat("yyyyMMddHHmm").format(new Date()),"13123123123123123123123");
        //String datafromFile = JsonFileUtil.getDatafromFile("river");
        //System.out.println(datafromFile);


    }
}
