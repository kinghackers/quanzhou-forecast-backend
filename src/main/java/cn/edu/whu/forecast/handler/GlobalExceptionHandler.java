//package cn.edu.whu.forecast.handler;
//
//import cn.edu.whu.forecast.common.ResultBean;
//import cn.edu.whu.forecast.enums.ResultCode;
//import cn.edu.whu.forecast.exception.BusinessException;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.stereotype.Component;
//import org.springframework.validation.BindException;
//import org.springframework.validation.BindingResult;
//import org.springframework.validation.FieldError;
//import org.springframework.web.bind.MethodArgumentNotValidException;
//import org.springframework.web.bind.MissingServletRequestParameterException;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import org.springframework.web.servlet.NoHandlerFoundException;
///**
//* 同一异常处理
//* @author fullangleblank
//* @since 2022/6/15
//*/
//@Slf4j
//@Component
//@RestControllerAdvice
//public class GlobalExceptionHandler {
//
//    /**
//     * NoHandlerFoundException 404 异常处理
//     */
//    @ExceptionHandler(value = NoHandlerFoundException.class)
//    @ResponseStatus(HttpStatus.OK)
//    public ResultBean<?> handleNotFoundException(NoHandlerFoundException e) throws Throwable {
//        // 404不需要处理 直接返回就行
//        return ResultBean.fail(ResultCode.NotFound);
//    }
//    /*
//    * NullPointerException
//    * */
//    @ExceptionHandler(value = NullPointerException.class)
//    @ResponseStatus(HttpStatus.OK)
//    public ResultBean<?> handleNullPointerException(NullPointerException e) throws Throwable {
//        log.error(e.getMessage());
//        return ResultBean.fail(ResultCode.InternalServerError);
//    }
//
//    /*
//    * MissingServletRequestParameterException
//    * */
//    @ExceptionHandler(value = MissingServletRequestParameterException.class)
//    @ResponseStatus(HttpStatus.OK)
//    public ResultBean<?> handleMissingServletRequestParameterException(MissingServletRequestParameterException e) throws Throwable {
//        log.error(e.getMessage());
//        return ResultBean.fail(ResultCode.InternalServerError);
//    }
//    /*
//    * BusinessException
//    * */
//    // 捕获自定义异常
//    @ExceptionHandler(value = BusinessException.class)
//    @ResponseStatus(HttpStatus.OK)
//    public ResultBean<?> handleBusinessException(BusinessException e) throws Throwable {
//        log.error(e.getResultCode().getError_message());
//        return ResultBean.fail(e.getResultCode());
//    }
//
//    /*
//    * Exception
//    * */
//    @ExceptionHandler(value = Exception.class)
//    @ResponseStatus(HttpStatus.OK)
//    public ResultBean<?> handleException(Exception e) throws Throwable {
//        log.error(e.getMessage());
//        return ResultBean.fail(ResultCode.InternalServerError);
//    }
//    /*
//    * MethodArgumentNotValidException
//    * */
//    @ExceptionHandler(value = MethodArgumentNotValidException.class)
//    @ResponseStatus(HttpStatus.OK)
//    public ResultBean<?> handleValidException(MethodArgumentNotValidException e) {
//        BindingResult bindingResult = e.getBindingResult();
//        StringBuilder message = new StringBuilder();
//        if (bindingResult.hasErrors()) {
//            FieldError fieldError = bindingResult.getFieldError();
//            if (fieldError != null) {
//                message.append(fieldError.getField()).append(fieldError.getDefaultMessage());
//            }
//        }
//        return ResultBean.fail(message.toString());
//    }
//    /*
//    * BindException
//    * */
//    @ExceptionHandler(value = BindException.class)
//    @ResponseStatus(HttpStatus.OK)
//    public ResultBean<?> handleValidException(BindException e) {
//        BindingResult bindingResult = e.getBindingResult();
//        StringBuilder message = new StringBuilder();
//        if (bindingResult.hasErrors()) {
//            FieldError fieldError = bindingResult.getFieldError();
//            if (fieldError != null) {
//                message.append(fieldError.getField()).append(fieldError.getDefaultMessage());
//            }
//        }
//        return ResultBean.fail(message.toString());
//    }
//    /*
//    * AccessDeniedException
//    * */
//    @ExceptionHandler(value = AccessDeniedException.class)
//    @ResponseStatus(HttpStatus.OK)
//    public ResultBean<?> handleAccessDeniedException(AccessDeniedException e){
//        log.error(e.getMessage());
//        return ResultBean.fail(ResultCode.Unauthorized);
//    }
//
////    @ResponseStatus(HttpStatus.OK)
////    @ExceptionHandler(MethodArgumentNotValidException.class)
////    public CommonResult<?> exceptionHandler(MethodArgumentNotValidException exception) {
////        BindingResult result = exception.getBindingResult();
////        StringBuilder stringBuilder = new StringBuilder();
////        if (result.hasErrors()) {
////            List<ObjectError> errors = result.getAllErrors();
////            errors.forEach(p -> {
////                FieldError fieldError = (FieldError) p;
////                log.warn("Bad Request Parameters: dto entity [{}],field [{}],message [{}]", fieldError.getObjectName(), fieldError.getField(), fieldError.getDefaultMessage());
////                stringBuilder.append(fieldError.getDefaultMessage());
////            });
////        }
////        return CommonResult.fail(stringBuilder.toString());
////    }
//}
