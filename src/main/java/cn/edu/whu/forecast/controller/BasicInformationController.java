package cn.edu.whu.forecast.controller;

import cn.edu.whu.forecast.common.ResultBean;
import cn.edu.whu.forecast.service.impl.BasicInformationImpl;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
* 基础信息接口
* @author fullangleblank
* @since 2022/6/27
*/
@RestController
@Api(tags = "基础信息")
@RequestMapping("/basic-info")
@CrossOrigin
public class BasicInformationController {
    @Resource
    private BasicInformationImpl basicInformation;

    @GetMapping("/getAllRainfallStation")
    public ResultBean<?> getAllRainfallStation(
            int pageNum,
            int pageSize
    ){
        return ResultBean.ok(basicInformation.getRainfallStation(pageNum,pageSize));
    }
}
