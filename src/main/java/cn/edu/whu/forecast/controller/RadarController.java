package cn.edu.whu.forecast.controller;

import cn.edu.whu.forecast.common.ResultBean;
import cn.edu.whu.forecast.entity.mysql.RadarRecord;
import cn.edu.whu.forecast.service.impl.RadarServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * 预报接口
 * @author fullangleblank
 * @since 2022/6/27
 */
@RestController
@Api(tags = "雷达")
@RequestMapping("/radar")
@CrossOrigin
public class RadarController {
    @Resource
    private RadarServiceImpl radarService;

    @GetMapping("/getRadarRecordList")
    private ResultBean<?> getRadarRecordList(){
        List<RadarRecord> allRadarRecord = radarService.getAllRadarRecord();
        List<String> res = new ArrayList<>();
        for(RadarRecord radarRecord : allRadarRecord) res.add(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(radarRecord.getTime()));
        return ResultBean.ok(res);
    }

    @GetMapping("/getRadarByTime")
    private ResultBean<?> getRadarByTime(
            @RequestParam("time") @DateTimeFormat(pattern = "yyyy-MM-dd HH")
                    LocalDateTime time){
        return ResultBean.ok(radarService.getRadarRecordByTime(time));
    }

    @PostMapping("/deleteRadarRecord")
    private ResultBean<?> deleteRadarRecord(
            @RequestParam("time")
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime time){
        return ResultBean.ok(radarService.deleteRadarRecord(time));
    }
}
