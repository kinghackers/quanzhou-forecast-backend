package cn.edu.whu.forecast.controller;

import cn.edu.whu.forecast.common.ResultBean;
import cn.edu.whu.forecast.service.impl.WaterLevelServiceImpl;
import io.swagger.annotations.Api;
import org.apache.ibatis.annotations.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

@RestController
@Api(tags = "水位数据相关")
@RequestMapping("/waterLevel")
@CrossOrigin
public class WaterLevelController {
    @Resource
    private WaterLevelServiceImpl waterLevelService;

    @GetMapping("/getWaterLevelRecordByNow")
    public ResultBean<?> getRainfallRecordByNow(
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
            @RequestParam("time") Date now
    ){
        now.setMinutes(0);
        now.setSeconds(0);
        return ResultBean.ok(waterLevelService.getWaterLevelRecordByNow(now));
    }

    @GetMapping("/getFlowRecordByNow")
    public ResultBean<?> getFlowRecordByNow(
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
            @RequestParam("time") Date now
    ){
        now.setSeconds(0);
        now.setMinutes(0);
        return ResultBean.ok(waterLevelService.getFlowRecordByNow(now));
    }
}
