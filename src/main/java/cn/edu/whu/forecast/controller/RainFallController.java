package cn.edu.whu.forecast.controller;

import cn.edu.whu.forecast.common.ResultBean;
import cn.edu.whu.forecast.entity.mysql.AvgRainfallAllForecast;
import cn.edu.whu.forecast.entity.mysql.PreAvgRainfallAll;
import cn.edu.whu.forecast.service.impl.AvgRainfallAllServiceImpl;
import cn.edu.whu.forecast.service.impl.RainfallServiceImpl;
import cn.edu.whu.forecast.util.HttpClientUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@RestController
@Api(tags = "降雨数据相关处理")
@RequestMapping("/rainfall")
@CrossOrigin
public class RainFallController {
    @Resource
    private AvgRainfallAllServiceImpl avgRainfallAllService;

    @Resource
    private RainfallServiceImpl rainfallService;


    @GetMapping("/getAvgRainfallAllForecastByNow")
    public ResultBean<?> getAvgRainfallAllForecast(

    ){
        return ResultBean.ok(avgRainfallAllService.getAvgRainfallAllForecast());
    }



    @PostMapping("/addAvgRainfallAllForecast")
    public ResultBean<?> addAvgRainfallAllForecast(){
        String s = HttpClientUtil.doGet("http://124.220.210.6:8082/radar");
        JSONObject object = JSONObject.parseObject(s);
        JSONArray datas = object.getJSONArray("data");
        for(int i=0; i<12; i++) {
            JSONArray data = datas.getJSONArray(i);
            AvgRainfallAllForecast avgRainfallAllForecast = new AvgRainfallAllForecast();
            avgRainfallAllForecast.setTime(LocalDateTime.parse(data.getString(0).replaceAll("T", " "), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            avgRainfallAllForecast.setRainfallA(data.getBigDecimal(1));
            avgRainfallAllForecast.setRainfallB(data.getBigDecimal(2));
            avgRainfallAllForecast.setRainfallC(data.getBigDecimal(3));
            avgRainfallAllForecast.setRainfallD(data.getBigDecimal(4));
            avgRainfallAllForecast.setRainfallE(data.getBigDecimal(5));
            avgRainfallAllForecast.setRainfallF(data.getBigDecimal(6));
            avgRainfallAllForecast.setRainfallG(data.getBigDecimal(7));
            avgRainfallAllForecast.setRainfallH(data.getBigDecimal(8));
            avgRainfallAllForecast.setRainfallI(data.getBigDecimal(9));
            avgRainfallAllForecast.setRainfallJ(data.getBigDecimal(10));
            avgRainfallAllForecast.setRainfallYc(data.getBigDecimal(11));
            avgRainfallAllForecast.setEe(data.getBigDecimal(12));


            avgRainfallAllService.addAvgRainfallAllForecast(avgRainfallAllForecast);
        }
        return ResultBean.ok();
    }

    @PostMapping("/addPreAvgRainfallAllMerge")
    public ResultBean<?> addPreAvgRainfallAll(){
        String s = HttpClientUtil.doGet("http://124.220.210.6:8082/merge");
        JSONObject object = JSONObject.parseObject(s);
        JSONArray datas = object.getJSONArray("data");
        for(int i=0; i<12; i++) {
            JSONArray data = datas.getJSONArray(i);
            PreAvgRainfallAll preAvgRainfallAll = new PreAvgRainfallAll();
            preAvgRainfallAll.setTime(LocalDateTime.parse(data.getString(0).replaceAll("T", " "), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            preAvgRainfallAll.setRainfallA(data.getBigDecimal(1));
            preAvgRainfallAll.setRainfallB(data.getBigDecimal(2));
            preAvgRainfallAll.setRainfallC(data.getBigDecimal(3));
            preAvgRainfallAll.setRainfallD(data.getBigDecimal(4));
            preAvgRainfallAll.setRainfallE(data.getBigDecimal(5));
            preAvgRainfallAll.setRainfallF(data.getBigDecimal(6));
            preAvgRainfallAll.setRainfallG(data.getBigDecimal(7));
            preAvgRainfallAll.setRainfallH(data.getBigDecimal(8));
            preAvgRainfallAll.setRainfallI(data.getBigDecimal(9));
            preAvgRainfallAll.setRainfallJ(data.getBigDecimal(10));
            preAvgRainfallAll.setRainfallYc(data.getBigDecimal(11));
            preAvgRainfallAll.setEe(data.getBigDecimal(12));


            avgRainfallAllService.addPreAvgRainfallAll(preAvgRainfallAll);
        }
        return ResultBean.ok();
    }

    @GetMapping("/addPreAvgRainfallAllMergeSingle")
    public ResultBean<?> addPreAvgRainfallAllMergeSingle(){
        String s = HttpClientUtil.doGet("http://124.220.210.6:8082/merge-single");
        JSONObject object = JSONObject.parseObject(s);
        JSONArray datas = object.getJSONArray("data");
        for(int i=0; i<datas.size(); i++){
            JSONArray data = datas.getJSONArray(i);
            PreAvgRainfallAll preAvgRainfallAll = new PreAvgRainfallAll();
            preAvgRainfallAll.setTime(LocalDateTime.parse(data.getString(0).replaceAll("T", " "), DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm")));
            preAvgRainfallAll.setRainfallA(data.getBigDecimal(1));
            preAvgRainfallAll.setRainfallB(data.getBigDecimal(2));
            preAvgRainfallAll.setRainfallC(data.getBigDecimal(3));
            preAvgRainfallAll.setRainfallD(data.getBigDecimal(4));
            preAvgRainfallAll.setRainfallE(data.getBigDecimal(5));
            preAvgRainfallAll.setRainfallF(data.getBigDecimal(6));
            preAvgRainfallAll.setRainfallG(data.getBigDecimal(7));
            preAvgRainfallAll.setRainfallH(data.getBigDecimal(8));
            preAvgRainfallAll.setRainfallI(data.getBigDecimal(9));
            preAvgRainfallAll.setRainfallJ(data.getBigDecimal(10));
            preAvgRainfallAll.setRainfallYc(data.getBigDecimal(11));
            preAvgRainfallAll.setEe(data.getBigDecimal(12));

            avgRainfallAllService.addPreAvgRainfallAll(preAvgRainfallAll);
        }
        return ResultBean.ok();
    }


    @GetMapping("/getRainfallByTime")
    public ResultBean<?> getRainfallByTime(
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
            @RequestParam("time") Date time){
        time.setMinutes(0);
        time.setSeconds(0);
        return ResultBean.ok(rainfallService.getRainfallByTime(time));
    }

//    @GetMapping("/getRainfallRecordByNow")
//    public ResultBean<?> getRainfallRecordByNow(
//            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//            @RequestParam("now") Date now
//    ){
//        now.setMinutes(0);
//        now.setSeconds(0);
//        return ResultBean.ok(rainfallService.getRainfallRecordByNow(now));
//    }

    @GetMapping("/getAvgRainfallByDistrictIdAndTime")
    public ResultBean<?> getAvgRainfallByDistrictIdAndTime(
            @RequestParam("districtId") Integer districtId,
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
            @RequestParam("now") Date now
    ){
        now.setMinutes(0);
        now.setSeconds(0);
        return ResultBean.ok(rainfallService.getAvgRainfallByDistrictIdAndTime(districtId,now));
    }

}
