package cn.edu.whu.forecast.vo.response;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class RainfallVo {
    private BigDecimal longitude;
    private BigDecimal latitude;
    private String stationName;
    private Date time;
    private BigDecimal rainfall;
}
