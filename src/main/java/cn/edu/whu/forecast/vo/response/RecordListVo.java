package cn.edu.whu.forecast.vo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecordListVo {
    private String time;
    private Integer has_merge;
}
