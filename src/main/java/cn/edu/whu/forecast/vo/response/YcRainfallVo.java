package cn.edu.whu.forecast.vo.response;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;


@Data
public class YcRainfallVo {
    private LocalDateTime time;
    private BigDecimal rainfall;
}
