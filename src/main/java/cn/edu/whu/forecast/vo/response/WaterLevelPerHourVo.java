package cn.edu.whu.forecast.vo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WaterLevelPerHourVo {
    private String time;
    private BigDecimal waterLevel;
}
