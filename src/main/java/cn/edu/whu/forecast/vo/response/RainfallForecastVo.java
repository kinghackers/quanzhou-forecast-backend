package cn.edu.whu.forecast.vo.response;

import com.alibaba.fastjson.JSONArray;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RainfallForecastVo {
    private String districtName;
    private List<RainfallPerHourVo> dataList;
    private JSONArray radar;
    private TotalVo total;
}
