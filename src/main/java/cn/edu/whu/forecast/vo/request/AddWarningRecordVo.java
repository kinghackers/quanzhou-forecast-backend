package cn.edu.whu.forecast.vo.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddWarningRecordVo {
    private BigDecimal criticalRainfall;
    private Integer warning;
    private Integer has_merge;
}
