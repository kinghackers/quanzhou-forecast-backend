package cn.edu.whu.forecast.vo.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
* 雨量站信息Vo
* @author fullangleblank
* @since 2022/6/21
*/
@ApiModel("雨量站VO")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StationInfoVo {
    @ApiModelProperty("站点名")
    private String name;

    @ApiModelProperty("经度")
    private BigDecimal longitude;

    @ApiModelProperty("纬度")
    private BigDecimal latitude;

}
