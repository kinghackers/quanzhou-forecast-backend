package cn.edu.whu.forecast.vo.response;

import lombok.Data;

@Data
public class MessageVo {
    private String username;

    private Integer type;

    private String content;

    private Integer isChecked;
}
