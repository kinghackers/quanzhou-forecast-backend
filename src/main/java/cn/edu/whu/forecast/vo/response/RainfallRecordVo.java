package cn.edu.whu.forecast.vo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RainfallRecordVo {
    private String districtName;
    private List<RainfallPerHourVo> dataList;
}
