package cn.edu.whu.forecast.vo.response;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class WarningRecordVo {

    private String stationName;

    private BigDecimal criticalRainfall;

    private Boolean warning;

    private LocalDateTime creatTime;
}
