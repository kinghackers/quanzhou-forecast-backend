package cn.edu.whu.forecast.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@ApiModel("日志修改vo")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class LogModifyVo {
    @ApiModelProperty("日志id")
    private Integer logId;
    @ApiModelProperty("更改后的状态")
    private String status;
}
