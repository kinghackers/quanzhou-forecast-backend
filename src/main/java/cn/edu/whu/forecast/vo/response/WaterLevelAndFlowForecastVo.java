package cn.edu.whu.forecast.vo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WaterLevelAndFlowForecastVo {
    private String time;
    private BigDecimal flow;
    private BigDecimal waterLevel;
}
