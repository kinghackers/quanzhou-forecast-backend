package cn.edu.whu.forecast.vo.response;

import lombok.Data;

@Data
public class UserVo {

    private String username;

    private String token;

    private Boolean role;

}
