package cn.edu.whu.forecast.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel("添加站点vo")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AddStationVo {
    @NotNull(message = "Type不能为空")
    @ApiModelProperty("Type")
    private Integer type;

    @NotBlank(message = "测站编码不能为空")
    @ApiModelProperty("测站编码")
    private String stationId;

    @NotBlank(message = "测站名不能为空")
    @ApiModelProperty("测站名称")
    private String stationName;

    @NotNull(message = "测站经度不能为空")
    @ApiModelProperty("经度")
    private BigDecimal longitude;

    @NotNull(message = "测站纬度不能为空")
    @ApiModelProperty("纬度")
    private BigDecimal latitude;

}
