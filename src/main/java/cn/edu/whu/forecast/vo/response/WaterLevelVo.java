package cn.edu.whu.forecast.vo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WaterLevelVo {
    private String stationName;
    List<WaterLevelPerHourVo> dataList;
}
