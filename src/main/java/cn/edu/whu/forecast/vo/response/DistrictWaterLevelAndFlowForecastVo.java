package cn.edu.whu.forecast.vo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DistrictWaterLevelAndFlowForecastVo {
    private String DistrictName;
    List<WaterLevelAndFlowForecastVo> dateList;
}
