package cn.edu.whu.forecast.util;

import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Component
public class RunMethodUtil<T> {
    /**
     * 执行有参数的方法
     * @param bean Spring Ioc中的bean
     * @param methodStr 需要执行的方法名
     * @param parameters 方法参数
     * @param parameterTypes 方法参数类型
     * @return 方法执行的结果
     */
    public T execute(Object bean, String methodStr, Object[] parameters, Class... parameterTypes) {
        // 使用反射执行 method
        //Class clazz = bean.getClass();
        try {
            Method method = bean.getClass().getMethod(methodStr, parameterTypes);
            return (T)method.invoke(bean, parameters);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 执行无参数的方法
     * @param bean Spring Ioc中的bean
     * @param methodStr 需要执行的方法名
     * @return 方法执行的结果
     */
    public T execute(Object bean, String methodStr) {
        // 使用反射执行 method
        //Class clazz = bean.getClass();
        try {
            Method method = bean.getClass().getMethod(methodStr);
            return (T)method.invoke(bean);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
