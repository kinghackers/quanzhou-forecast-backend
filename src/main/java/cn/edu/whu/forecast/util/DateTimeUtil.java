package cn.edu.whu.forecast.util;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class DateTimeUtil {


    /**
     * 将输入的时间忽略其分钟值和秒值返回
     * @param source 输入时间
     * @return 忽略分、秒后的时间
     */
    public static LocalDateTime ignoreMinute(LocalDateTime source) {
        return LocalDateTime.of(source.getYear(), source.getMonth(), source.getDayOfMonth(), source.getHour(), 0);
    }

    /**
     * 将输入的时间忽略其小时值、分钟值和秒值返回
     * @param source 输入时间
     * @return 忽略时、分、秒后的时间
     */
    public static LocalDateTime ignoreHour(LocalDateTime source) {
        return LocalDateTime.of(source.getYear(), source.getMonth(), source.getDayOfMonth(), 0, 0);
    }

    /**
     * 将输入的时间忽略其小时值、分钟值和秒值返回
     * @param source 输入时间
     * @return 忽略时、分、秒后的时间
     */
    public static LocalDateTime ignoreDayAndSet1Day(LocalDateTime source) {
        return LocalDateTime.of(source.getYear(), source.getMonth(), 1, 0, 0);
    }



    /**
     * Date类型转LocalDateTime类型的
     * @param date 日期
     * @return LocalDateTime类型
     */
    public static LocalDateTime DateToLocalDatetime(Date date) {
        return Instant.ofEpochMilli(date.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
}
