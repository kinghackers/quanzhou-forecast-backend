package cn.edu.whu.forecast.util;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.internal.types.JavaTypeResolverDefaultImpl;

import java.time.LocalDateTime;

/**
 * @Author DKR
 * @Date 2021/7/22 11:03
 * @description 自定义mybatis类型转换规则
 */

public class MyJavaTypeResolver extends JavaTypeResolverDefaultImpl {

    /**
     * 将timestamp转换为localDateTime
     */
    public MyJavaTypeResolver() {
        super();
        super.typeMap.put(93, new JavaTypeResolverDefaultImpl.JdbcTypeInformation("TIMESTAMP", new FullyQualifiedJavaType(LocalDateTime.class.getName())));
    }
}
