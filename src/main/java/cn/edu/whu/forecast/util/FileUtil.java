package cn.edu.whu.forecast.util;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;


/**
 * @Author: Wang Lang
 * @Date: 2021/11/8 9:32
 * @Description: 文件操作工具类
 */
public class FileUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    /**
     * @Description 下载网页端的文件
     * @Param urlStr    文件url
     * @Param dir    下载的文件所在的文件
     * @Param fileName    下载后的文件的文件名
     * @Return java.io.File
     */
    public static File downloadOnce(String urlStr, String dir, String fileName) throws MalformedURLException, FileNotFoundException, UnknownHostException {
        URL url;
        try {
            url = new URL(urlStr);
        } catch (MalformedURLException e) {
            LOGGER.info("无效的url：" + urlStr);
            throw e;
        }
        File directory = new File(dir);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        File file = new File(directory, fileName);
        boolean downloadSuccess = false;
        int count = 0;
        while (!downloadSuccess && count < 10) {
            try {
                FileUtils.copyURLToFile(url, file);
            }catch (FileNotFoundException e){
                throw e;
            }catch (UnknownHostException e) {
                LOGGER.info("java.net.UnknownHostException: " + e.getMessage());
                throw e;
            } catch (Exception e) {
                e.printStackTrace();
            }
            downloadSuccess = file.length() > 0L;
            count++;
            if (!downloadSuccess) {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return downloadSuccess ? file : null;
    }

    /**
     * @Description 下载网页端的文件
     * @Param urlStr    文件url
     * @Param dir    下载的文件所在的文件
     * @Param fileName    下载后的文件的文件名
     * @Return java.io.File
     */
    public static File downloadFile(String urlStr, String dir, String fileName) throws FileNotFoundException, MalformedURLException, UnknownHostException {
        URL url;
        try {
            url = new URL(urlStr);
        } catch (MalformedURLException e) {
            LOGGER.info("无效的url：" + urlStr);
            throw e;
        }
        File directory = new File(dir);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        File file = new File(directory, fileName);
        boolean downloadSuccess = false;
        Thread currentThread = Thread.currentThread();
        while (!downloadSuccess && !currentThread.isInterrupted()) {
            try {
                FileUtils.copyURLToFile(url, file, 30 * 1000, 180 * 1000);
            }catch (FileNotFoundException e){
                throw e;
            } catch (UnknownHostException e) {
                LOGGER.info("java.net.UnknownHostException: " + e.getMessage());
                throw e;
            }catch (Exception e) {
                e.printStackTrace();
            }
            downloadSuccess = file.length() > 0L;
            if (!downloadSuccess) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    //重置线程中断标志
                    Thread.currentThread().interrupt();
                    // 线程被interrupt,表示下载超时
                    //LOGGER.info("文件" + fileName + "下载超时");
                }
            }
        }
        return downloadSuccess ? file : null;
    }

    /**
     * @Description 删除目录
     * @Param path
     * @Return boolean
     */
    public static void deleteDirectory(String path) {
        try {
            FileUtils.deleteDirectory(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @Description 删除目录
     * @Param     directory
     * @Return void
     */
    public static void deleteDirectory(File directory){
        try {
            FileUtils.deleteDirectory(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
