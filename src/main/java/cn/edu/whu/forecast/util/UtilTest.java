package cn.edu.whu.forecast.util;

import cn.hutool.core.date.DateTime;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilTest {
    public static void main(String[] args) throws Exception {
        OutputStream outputStreamExcel = null;
        File tmpFile = new File("e:\\details.xlsx");
        if (!tmpFile.getParentFile().exists()) {
            tmpFile.getParentFile().mkdirs();//创建目录
        }
        if(!tmpFile.exists()) {
            tmpFile.createNewFile();//创建文件
        }
        Workbook workbook = null;
        workbook = new XSSFWorkbook();//创建Workbook对象(excel的文档对象)
        Sheet sheet1 = workbook.createSheet("Sheet1");// 建建sheet对象（excel的表单）
        // 设置单元格字体
        Font headerFont = workbook.createFont(); // 字体
        headerFont.setFontHeightInPoints((short)14);
        headerFont.setFontName("黑体");
        // 设置单元格边框及颜色
        CellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.valueOf((short)1));
        style.setBorderLeft(BorderStyle.valueOf((short)1));
        style.setBorderRight(BorderStyle.valueOf((short)1));
        style.setBorderTop(BorderStyle.valueOf((short)1));
        style.setWrapText(true);

        Row row1 = sheet1.createRow(0);
        row1.createCell(0).setCellValue("LGTD");
        row1.createCell(1).setCellValue(118.088333);
        row1.createCell(2).setCellValue(118.028056);
        row1.createCell(3).setCellValue(118.202778);

        Row row2 = sheet1.createRow(1);
        row2.createCell(0).setCellValue("LTTD");
        row2.createCell(1).setCellValue(24.989444);
        row2.createCell(2).setCellValue(25.278056);
        row2.createCell(3).setCellValue(24.930556);

        Row row3 = sheet1.createRow(2);
        row3.createCell(0).setCellValue("STNM");
        row3.createCell(1).setCellValue("八马茶厂q");
        row3.createCell(2).setCellValue("白濑乡q");
        row3.createCell(3).setCellValue("坂头水库q");


        for(int i=3; i<10; i++){
            Row row = sheet1.createRow(i);
            row.createCell(0).setCellValue(new SimpleDateFormat("yyyyMMddHHmm").format(new Date()));
            row.createCell(1).setCellValue(i+0.0);
            row.createCell(2).setCellValue(i+0.0);
            row.createCell(3).setCellValue(i+0.0);
        }

        outputStreamExcel = new FileOutputStream(tmpFile);
        workbook.write(outputStreamExcel);
        outputStreamExcel.flush();
        outputStreamExcel.close();
    }
}
