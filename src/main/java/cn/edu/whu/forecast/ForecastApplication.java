package cn.edu.whu.forecast;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages = {"cn.edu.whu.forecast.mapper"})
@EnableScheduling
public class ForecastApplication {
    public static void main(String[] args) {
        SpringApplication.run(ForecastApplication.class);
    }
}
