package cn.edu.whu.forecast.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 响应状态枚举
 * @author fullangleblank
 * @since 2022/6/14
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum ResultCode {


    /**
     * 请求成功
     * */
    OK(200,"SUCCESS"),

    /**
     * 服务器异常
     * */
    ERROR(500,"未知异常，请联系管理员！"),

    /**
     * 参数错误
     * */
    PARAM_ERROR(400,"非法参数！"),

    /**
     * 拒绝访问
     * */
    FORBIDDEN(403,"拒绝访问！"),

    AlreadyForecast(202,"每小时只能预报一次"),


    SuccessNoData(201, "成功但未查询到数据"),
    BadRequest(400, "请求有误！"),
    Unauthorized(401, "用户未授权！"),
    NotFound(404, "找不到请求地址！"),
    InternalServerError(500, "内部服务器错误！"),
    InsertError(801, "新增错误"),
    UpdateError(802, "更新错误"),
    DeleteError(803, "删除错误"),
    TimeError(804, "时间参数错误"),
    ParamsError(805, "参数传递错误"),
    pythonInterfaceError(806, "调用python接口出错"),
    // 基础信息查询模块错误码
    StationNotFound(4001, "站点不存在"),
    StationExists(4002, "站点已存在"),
    CurveNotFound(4003, "曲线不存在"),
    TypeError(4004, "type参数有误"),
    //用户模块错误码
    UserLoginVerifyFail(1001, "用户名或密码错误"),
    UserUnLogin(1002, "用户未登录或登录过期"),
    UserUpdateToken(1003, "需要续期token"),
    UserAlreadyExist(10000000,"用户已存在"),
    UserDoesNotExist(10000001,"用户不存在"),
    VerificationCodeError(10000002,"验证码错误"),
    // 洪水预报模块错误码
    ForecastPlanNotFound(9002, "找不到对应的预报方案"),
    ForecastPlanNotSaved(9003, "没有未保存的预报方案"),
    FloodRecordNotFound(9007, "找不到此洪水记录"),
    InvalidTime(9001, "时间无效"),
    ManualForecastSaveFail(9006, "手动预报结果保存失败"),
    AnotherForecastRunning(9008, "用户有手动预报正在进行"),
    //参数传递错误
    ModelIdError(10001, "模型ID无效"),
    ParameterIsUse(10002, "模型参数正在被使用"),
    //气象预报模块
    QueryGraphError(11001, "获取气象预报图形展示错误"),
    GetForecastStatusError(11009, "获取模型手动预报状态出错"),
    // 手动采集模块
    GetChinaError(12001, "采集中国气象预报失败"),
    GetUSAMidError(12002, "采集美国短中期气象失败"),
    DateError(12004, "日期有误，请选择近一周的日期"),
    AcquireLockFail(12005, "有人正在进行气象采集"),
    FactorFail(12006, "气象因子采集失败"),
    SeaFactorFail(12006, "海温数据采集失败"),
    GetUSALongError(12003, "采集美国长期气象失败");




    private int code;
    private String msg;


}
