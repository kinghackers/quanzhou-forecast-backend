package cn.edu.whu.forecast.dto;

import lombok.Data;

@Data
public class UserDetailsDto {
    private Long id;
    private String username;
    private String name;
}
