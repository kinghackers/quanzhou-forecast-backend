package cn.edu.whu.forecast.cache;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CacheEntity {
    /**
     * 数据主体
     */
    private Object data;

    /**
     * 超时时长，0永不超时
     */
    private Long timeout;

    /**
     * 缓存设置初始时间
     */
    private Long initialTime;
}
