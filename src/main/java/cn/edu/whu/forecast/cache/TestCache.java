package cn.edu.whu.forecast.cache;

import cn.edu.whu.forecast.cache.CacheListener;
import cn.edu.whu.forecast.cache.CacheManager;
import cn.edu.whu.forecast.cache.CacheManagerImpl;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class TestCache {
    public static void main(String[] args) {
        CacheManager cacheManager = CacheManagerImpl.getInstance();
        cacheManager.put("testtimeout", "kkkkkkkkk", 20 * 1000L);
        cacheManager.put("test1", "111111111", 0L);
        cacheManager.put("test2", "222222222222222", 100 * 1000L);
        cacheManager.put("test3", "33333333333333", 0L);
        CacheListener cacheListener = new CacheListener(cacheManager);
        cacheListener.startListen();

        System.out.println("getCacheAll : " + Collections.singletonList(cacheManager.getCacheAll()));
        System.out.println("getCacheKeyAll : " + Collections.singletonList(cacheManager.getKeys()));
        System.out.println("getKey : " + Collections.singletonList(cacheManager.getCacheByKey("test1")));
        System.out.println("isExist false: " + Collections.singletonList(cacheManager.isExist("test")));
        System.out.println("isExist true: " + Collections.singletonList(cacheManager.isExist("test1")));
        System.out.println("istimeOut false: " + Collections.singletonList(cacheManager.isTimeOut("test1")));

        try {
            TimeUnit.SECONDS.sleep(30);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println("isExist : " + Collections.singletonList(cacheManager.isExist("testtimeout")));
    }
}
