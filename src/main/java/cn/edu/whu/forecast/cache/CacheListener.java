package cn.edu.whu.forecast.cache;

import java.util.Set;

public class CacheListener {
    private final CacheManager cacheManager;

    public CacheListener(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public void startListen() {
        new Thread(() -> {
            while(true) {
                Set<String> keys = cacheManager.getKeys();

                for (String key : keys) {
                    if(cacheManager.isTimeOut(key)) {
                        cacheManager.clearByKey(key);
                        System.out.println(key + " 被清除了");
                    }
                }

                try {
                    Thread.sleep(5000);
                    System.out.println("休息5s再检查");
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
