package cn.edu.whu.forecast.exception;

import cn.edu.whu.forecast.enums.ResultCode;
/**
* 自定义全局异常类
* @author fullangleblank
* @since 2022/6/15
*/
public class BusinessException extends RuntimeException {
    private ResultCode resultCode;

    public BusinessException(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    public void setResultCode(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    public ResultCode getResultCode() {
        return resultCode;
    }

}