package cn.edu.whu.forecast.entity.mysql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserInfo {
    private Integer id;

    private String name;

    private String username;

    private String password;

    private Boolean role;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;
}