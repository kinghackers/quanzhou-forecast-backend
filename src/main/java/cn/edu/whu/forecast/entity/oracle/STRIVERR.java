package cn.edu.whu.forecast.entity.oracle;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class STRIVERR {
    private String stcd;

    private Date tm;

    private String type;

    private BigDecimal z;

    private BigDecimal q;

    private BigDecimal xsa;

    private BigDecimal xsavv;

    private BigDecimal xsmxv;

    private String flwchrcd;

    private String wptn;

    private String msqmt;

    private String msamt;

    private String msvmt;

}