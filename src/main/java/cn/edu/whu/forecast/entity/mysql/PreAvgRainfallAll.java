package cn.edu.whu.forecast.entity.mysql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PreAvgRainfallAll {
    private LocalDateTime time;

    private BigDecimal rainfallA;

    private BigDecimal rainfallB;

    private BigDecimal rainfallC;

    private BigDecimal rainfallD;

    private BigDecimal rainfallE;

    private BigDecimal rainfallF;

    private BigDecimal rainfallG;

    private BigDecimal rainfallH;

    private BigDecimal rainfallI;

    private BigDecimal rainfallJ;

    private BigDecimal rainfallYc;

    private BigDecimal ee;
}