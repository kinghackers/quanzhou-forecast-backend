package cn.edu.whu.forecast.entity.mysql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WarningRecord {
    private Integer id;

    private String stationName;

    private BigDecimal criticalRainfall;

    private Integer warning;

    private LocalDateTime creatTime;

    private LocalDateTime updateTime;

    private Integer has_merge;

}