package cn.edu.whu.forecast.entity.mysql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PreStationRainfall {
    private Integer id;

    private String stcd;

    private BigDecimal rainfall;

    private LocalDateTime time;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

}