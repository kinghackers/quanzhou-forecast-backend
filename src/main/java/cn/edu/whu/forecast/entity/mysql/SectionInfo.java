package cn.edu.whu.forecast.entity.mysql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SectionInfo {
    private Integer id;

    private Integer districtId;

    private LocalDateTime creatTime;

    private LocalDateTime updateTime;

    private BigDecimal longitude;

    private BigDecimal latitude;
}