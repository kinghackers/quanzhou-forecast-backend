package cn.edu.whu.forecast.entity.mysql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Message {
    private Integer messageId;

    private LocalDateTime time;

    private String username;

    private Integer type;

    private String content;

    private Integer isChecked;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

}