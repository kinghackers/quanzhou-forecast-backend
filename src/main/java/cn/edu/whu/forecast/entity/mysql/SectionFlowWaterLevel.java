package cn.edu.whu.forecast.entity.mysql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SectionFlowWaterLevel {
    private Integer id;

    private Integer sectionId;

    private BigDecimal flow;

    private BigDecimal waterLevel;

    private LocalDateTime creatTime;

    private LocalDateTime updateTime;
}