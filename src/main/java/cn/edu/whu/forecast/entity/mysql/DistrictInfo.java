package cn.edu.whu.forecast.entity.mysql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DistrictInfo {
    private Integer id;

    private String name;

    private Integer area;

}