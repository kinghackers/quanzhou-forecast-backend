package cn.edu.whu.forecast.entity.oracle;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class STSTBPRPB {
    private String stcd;

    private String addvcd;

    private String stnm;

    private String rvnm;

    private String hnnm;

    private String bsnm;

    private BigDecimal lgtd;

    private BigDecimal lttd;

    private String stlc;

    private String dtmnm;

    private BigDecimal dtmel;

    private BigDecimal dtpr;

    private String sttp;

    private String frgrd;

    private String esstym;

    private String bgfrym;

    private String atcunit;

    private String admauth;

    private String locality;

    private String stbk;

    private Short stazt;

    private BigDecimal dstrvm;

    private Integer drna;

    private String phcd;

    private String usfl;

    private String comments;

    private Date moditime;
}