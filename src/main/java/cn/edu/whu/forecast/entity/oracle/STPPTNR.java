package cn.edu.whu.forecast.entity.oracle;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class STPPTNR {
    private String stcd;

    private Date tm;

    private String type;

    private BigDecimal drp;

    private BigDecimal intv;

    private BigDecimal pdr;

    private BigDecimal dyp;

    private String wth;

    private BigDecimal gpsDrp;

    private BigDecimal dbDrp;

    private BigDecimal cdbDrp;

}