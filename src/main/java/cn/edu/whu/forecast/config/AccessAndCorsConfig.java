package cn.edu.whu.forecast.config;

import cn.edu.whu.forecast.interceptor.CORSInterceptor;
import cn.edu.whu.forecast.interceptor.JwtInterceptor;
import cn.edu.whu.forecast.interceptor.SystemAccessInterceptor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
@EnableAutoConfiguration(exclude = {
        SecurityAutoConfiguration.class
})
public class AccessAndCorsConfig implements WebMvcConfigurer {

//    @Resource
//    SystemAccessInterceptor systemAccessInterceptor;

    @Resource
    CORSInterceptor corsInterceptor;

    @Resource
    JwtInterceptor jwtInterceptor;
//    public SystemAccessInterceptor init(){
//        return new SystemAccessInterceptor();
//    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(systemAccessInterceptor)
//                .addPathPatterns("/forecast/**")
//                .excludePathPatterns("/basic-info/**");
        registry.addInterceptor(corsInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/test/**")
                .excludePathPatterns("/user/code");

        registry.addInterceptor(jwtInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/test/**")
                .excludePathPatterns("/user/code");
    }


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowCredentials(true)
                .allowedOrigins("*")
                .allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS")
                .allowedHeaders("*");
    }

    //springboot2.x security关闭验证

}
