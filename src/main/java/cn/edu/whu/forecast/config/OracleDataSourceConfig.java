package cn.edu.whu.forecast.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

/**
 * @author LXP
 * @version 1.0
 * @date 2021/12/30 10:08
 * 配置Oracle数据源
 **/
@Configuration
@MapperScan(basePackages = "cn.edu.whu.forecast.mapper.oracle", sqlSessionFactoryRef = "oracleSqlSessionFactory")
public class OracleDataSourceConfig {
    @Value("${spring.datasource.oracle.jdbc-url}")
    private String url;

    @Value("${spring.datasource.oracle.username}")
    private String user;

    @Value("${spring.datasource.oracle.password}")
    private String password;

    @Value("${spring.datasource.oracle.driver-class-name}")
    private String driverClass;

    @Value("${spring.datasource.oracle.druid.test-on-borrow}")
    private boolean testOnBorrow;

    @Value("${spring.datasource.oracle.druid.test-while-idle}")
    private boolean testWhileIdle;

    @Value("${spring.datasource.oracle.druid.validationQuery}")
    private String validationQuery;

    @Value("${spring.datasource.oracle.druid.minIdle}")
    private int minIdle;

    @Value("${spring.datasource.oracle.druid.initialSize}")
    private int initialSize;

    @Value("${spring.datasource.oracle.druid.maxActive}")
    private int maxActive;

    @Value("${spring.datasource.oracle.druid.maxWait}")
    private int maxWait;

    @Value("${spring.datasource.oracle.druid.minEvictableIdleTimeMillis}")
    private int minEvictableIdleTimeMillis;

    @Bean("oracleDataSource")
//    @ConfigurationProperties(prefix = "spring.datasource.oracle")  //读取application.yml中的配置参数映射成为一个对象
    public DataSource getOracleDataSource(){
        DruidDataSource dataSource = new DruidDataSource();

        dataSource.setDriverClassName(driverClass);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);

        dataSource.setTestOnBorrow(testOnBorrow);
        dataSource.setTestWhileIdle(testWhileIdle);
        dataSource.setTestOnReturn(true);

        dataSource.setMaxActive(maxActive);
        dataSource.setInitialSize(initialSize);
        dataSource.setMinIdle(minIdle);
        dataSource.setMaxWait(maxWait);
        dataSource.setTimeBetweenEvictionRunsMillis(60000);
        dataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);

        dataSource.setKeepAlive(true);

        dataSource.setValidationQuery(validationQuery);
        return dataSource;
    }

    @Primary
    @Bean("oracleSqlSessionFactory")
    public SqlSessionFactory db1SqlSessionFactory(@Qualifier("oracleDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:mapper/oracle/*.xml"));
        return bean.getObject();
    }

    @Primary
    @Bean("oracleSqlSessionTemplate")
    public SqlSessionTemplate db1SqlSessionTemplate(@Qualifier("oracleSqlSessionFactory") SqlSessionFactory sqlSessionFactory){
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
