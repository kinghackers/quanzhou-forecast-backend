package cn.edu.whu.forecast.schudule.jobs;


import cn.edu.whu.forecast.util.HttpClientUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;



public class CacheJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        HttpClientUtil.doGet("http://10.35.80.145:8888/test/Cache");
    }
}
