package cn.edu.whu.forecast.schudule.jobs;


import cn.edu.whu.forecast.controller.ForecastController;
import cn.edu.whu.forecast.service.impl.WarningRecordServiceImpl;
import cn.edu.whu.forecast.util.ApplicationContextUtil;
import cn.edu.whu.forecast.vo.request.AddWarningRecordVo;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;


import javax.annotation.Resource;
import java.math.BigDecimal;
@Component("testServiceJob")
public class TestServiceJob implements Job {


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        WarningRecordServiceImpl warningRecordServiceImpl = (WarningRecordServiceImpl) ApplicationContextUtil.getBean("WarningRecordServiceImpl");
        //warningRecordServiceImpl.addWarningRecord(new AddWarningRecordVo(new BigDecimal("120.2112"),1));

    }
}
