package cn.edu.whu.forecast.schudule;

import cn.edu.whu.forecast.schudule.jobs.*;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class QuartzService {
    private static final Logger logger_crawler = LoggerFactory.getLogger(CrawlerJob.class);
    //private static final Logger logger_radar = LoggerFactory.getLogger(RadarJob.class);
    //private static final Logger logger_Service = LoggerFactory.getLogger(TestServiceJob.class);
    private static final Logger logger_addService = LoggerFactory.getLogger(AddRainfallCellJob.class);
    private static final Logger logger_cacheService = LoggerFactory.getLogger(CacheJob.class);
    private static final Logger logger_delPic = LoggerFactory.getLogger(DelJob.class);



    private final String Crawler_CRON_TIME = "0 0/6 * * * ?";
    private final String TRIGGER_Crawler_NAME = "Crawler";


    private final String Radar_CRON_TIME = "0 20 0/1 * * ? *";
    private final String TRIGGER_Radar_NAME = "Radar";

    private final String Service_CRON_TIME = "0/10 * * * * ? *";
    private final String TRIGGER_Service_NAME = "AddWarning";

    private final String addService_CRON_TIME = "0 10 0/1 * * ?";
    private final String TRIGGER_addService_NAME = "addService";

    private final String cacheService_CRON_TIME = "0 12 0/1 * * ?";
    private final String TRIGGER_cacheService_NAME = "cacheService";

    private final String delPic_CRON_TIME = "0 0 2 * * ? *";
    private final String TRIGGER_delPic_NAME = "delService";

    @PostConstruct
    public void taskInit() {


        logger_crawler.info("=========系统爬虫定时任务初始化成功========");
        //logger_radar.info("=========系统请求定时请求雷达任务成功========");
        //logger_Service.info("=========系统请求定时请求雷达任务成功========");
        logger_addService.info("=========系统请求定时请求添加降雨数据任务成功========");
        logger_cacheService.info("=========系统请求定时计算并缓存雷达数据任务成功========");
        logger_delPic.info("=========系统请求定时删除雷达图片任务成功========");


        try {


            SchedulerFactory schedulerFactory = new StdSchedulerFactory();
            Scheduler scheduler = schedulerFactory.getScheduler();


            TriggerKey trigger_crawler = TriggerKey.triggerKey(TRIGGER_Crawler_NAME, Scheduler.DEFAULT_GROUP);
            TriggerKey trigger_radar = TriggerKey.triggerKey(TRIGGER_Radar_NAME, Scheduler.DEFAULT_GROUP);
            TriggerKey trigger_Service = TriggerKey.triggerKey(TRIGGER_Service_NAME, Scheduler.DEFAULT_GROUP);
            TriggerKey trigger_addService = TriggerKey.triggerKey(TRIGGER_addService_NAME, Scheduler.DEFAULT_GROUP);
            TriggerKey trigger_cacheService = TriggerKey.triggerKey(TRIGGER_cacheService_NAME, Scheduler.DEFAULT_GROUP);
            TriggerKey trigger_delPic = TriggerKey.triggerKey(TRIGGER_delPic_NAME, Scheduler.DEFAULT_GROUP);

            JobDetail crawler = JobBuilder
                    .newJob(CrawlerJob.class)
                    .withDescription("定制化爬虫任务：每六分钟爬取一次雷达数据")
                    .withIdentity(TRIGGER_Crawler_NAME, Scheduler.DEFAULT_GROUP)
                    .build();

            JobDetail radar = JobBuilder
                    .newJob(RadarJob.class)
                    .withDescription("定制化Radar接口请求任务，每小时请求一次")
                    .withIdentity(TRIGGER_Radar_NAME, Scheduler.DEFAULT_GROUP)
                    .build();

            JobDetail Service = JobBuilder
                    .newJob(TestServiceJob.class)
                    .withDescription("测试测试测试测试")
                    .withIdentity(TRIGGER_Service_NAME, Scheduler.DEFAULT_GROUP)
                    .build();

            JobDetail addService = JobBuilder
                    .newJob(AddRainfallCellJob.class)
                    .withDescription("定时添加雨量站数据")
                    .withIdentity(TRIGGER_addService_NAME, Scheduler.DEFAULT_GROUP)
                    .build();

            JobDetail cacheService = JobBuilder
                    .newJob(CacheJob.class)
                    .withDescription("定时缓存雷达降雨数据")
                    .withIdentity(TRIGGER_cacheService_NAME, Scheduler.DEFAULT_GROUP)
                    .build();

            JobDetail delPic = JobBuilder
                    .newJob(DelJob.class)
                    .withDescription("定时删除雷达图片")
                    .withIdentity(TRIGGER_delPic_NAME, Scheduler.DEFAULT_GROUP)
                    .build();


            CronScheduleBuilder cron_crawler = CronScheduleBuilder.cronSchedule(Crawler_CRON_TIME);
            CronScheduleBuilder cron_radar = CronScheduleBuilder.cronSchedule(Radar_CRON_TIME);
            CronScheduleBuilder cron_Service = CronScheduleBuilder.cronSchedule(Service_CRON_TIME);
            CronScheduleBuilder cron_addService = CronScheduleBuilder.cronSchedule(addService_CRON_TIME);
            CronScheduleBuilder cron_cacheService = CronScheduleBuilder.cronSchedule(cacheService_CRON_TIME);
            CronScheduleBuilder cron_delPic = CronScheduleBuilder.cronSchedule(delPic_CRON_TIME);


            CronTrigger cronTrigger_crawler = TriggerBuilder
                    .newTrigger()
                    .withIdentity(trigger_crawler)
                    .withSchedule(cron_crawler)
                    .build();

            CronTrigger cronTrigger_radar = TriggerBuilder
                    .newTrigger()
                    .withIdentity(trigger_radar)
                    .withSchedule(cron_radar)
                    .build();

            CronTrigger cronTrigger_Service = TriggerBuilder
                    .newTrigger()
                    .withIdentity(trigger_Service)
                    .withSchedule(cron_Service)
                    .build();

            CronTrigger cronTrigger_addService = TriggerBuilder
                    .newTrigger()
                    .withIdentity(trigger_addService)
                    .withSchedule(cron_addService)
                    .build();

            CronTrigger cronTrigger_cacheService = TriggerBuilder
                    .newTrigger()
                    .withIdentity(trigger_cacheService)
                    .withSchedule(cron_cacheService)
                    .build();

            CronTrigger cronTrigger_delPic = TriggerBuilder
                    .newTrigger()
                    .withIdentity(trigger_delPic)
                    .withSchedule(cron_delPic)
                    .build();


            scheduler.scheduleJob(crawler, cronTrigger_crawler);
            scheduler.scheduleJob(addService, cronTrigger_addService);
            scheduler.scheduleJob(cacheService, cronTrigger_cacheService);
            scheduler.scheduleJob(delPic, cronTrigger_delPic);
            //scheduler.scheduleJob(radar, cronTrigger_radar);
            //scheduler.scheduleJob(Service, cronTrigger_Service);
            scheduler.start();
            logger_crawler.info("=========初始化爬虫定时任务加载完成=========");
            logger_addService.info("=========初始化添加降雨数据定时任务加载完成=========");
            logger_cacheService.info("=========初始化缓存定时任务加载完成=========");
            logger_delPic.info("=========初始化删除雷达图片定时任务加载完成=========");
            //logger_radar.info("=========初始化定时任务加载完成=========");
            //logger_Service.info("=========初始化定时任务加载完成=========");
        } catch (Exception e) {
            logger_crawler.info("==============初始化加载爬虫定时任务失败==============" + e);
            logger_addService.info("==============初始化加载添加降雨数据定时任务失败==============" + e);
            logger_cacheService.info("==============初始化加载缓存定时任务失败==============" + e);
            logger_delPic.info("==============初始化加载删除雷达图片定时任务失败==============" + e);
            //logger_radar.info("==============初始化加载定时任务失败==============" + e);
            //logger_Service.info("==============初始化加载定时任务失败==============" + e);
            e.printStackTrace();
        }
    }
}