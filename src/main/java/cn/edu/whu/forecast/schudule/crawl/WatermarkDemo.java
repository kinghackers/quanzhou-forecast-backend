package cn.edu.whu.forecast.schudule.crawl;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

public class WatermarkDemo {
    File imageFile;
    String path;

    public WatermarkDemo(String path) {
        this.imageFile = new File(path);
        this.path = path;
    }

    public void clearWaterMark() throws IOException {
        BufferedImage bi = ImageIO.read(imageFile);
        //水印替换成白色
        Color disColor = new Color(255, 255, 255);
        for (int i = 0; i < bi.getWidth(); i++) {
            for (int j = 0; j < bi.getHeight(); j++) {
                int color = bi.getRGB(i, j);
                Color oriColor = new Color(color);
                int red = oriColor.getRed();
                int greed = oriColor.getGreen();
                int blue = oriColor.getBlue();
                //245,245,245是灰色  这里是把当前像素点由灰色替换成白色
                if (red == 245 && greed == 245 && blue == 245) {
                    bi.setRGB(i, j, disColor .getRGB());
                }
            }
        }
        String type = path.substring(path.lastIndexOf(".") + 1, path.length());
        Iterator<ImageWriter> it = ImageIO.getImageWritersByFormatName(type);
        ImageWriter writer = it.next();
        File f = new File("C:\\Users\\Administrator\\Desktop\\RADA_CHN_DOR_L3_MOCPUP_CR.20220701-Z9591\\Z_RADA_I_Z9591_20220701104800_P_DOR_RDCP_CR.PNG");
        f.getParentFile().mkdirs();
        ImageOutputStream ios = ImageIO.createImageOutputStream(f);
        writer.setOutput(ios);
        writer.write(bi);
        bi.flush();
        ios.flush();
        ios.close();
    }

    public static void main(String[] args) throws IOException {
        WatermarkDemo t = new WatermarkDemo("C:\\Users\\Administrator\\Desktop\\RADA_CHN_DOR_L3_MOCPUP_CR.20220701-Z9591\\Z_RADA_I_Z9591_20220701104800_P_DOR_RDCP_CR.PNG");
        t.clearWaterMark();
    }
}