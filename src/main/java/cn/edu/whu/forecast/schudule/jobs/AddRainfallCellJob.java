package cn.edu.whu.forecast.schudule.jobs;

import cn.edu.whu.forecast.service.RainfallService;
import cn.edu.whu.forecast.service.impl.RainfallServiceImpl;
import cn.edu.whu.forecast.service.impl.WarningRecordServiceImpl;
import cn.edu.whu.forecast.util.ApplicationContextUtil;
import cn.edu.whu.forecast.util.HttpClientUtil;
import cn.edu.whu.forecast.vo.response.RainfallVo;
import lombok.SneakyThrows;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;

@Component("AddRainfallCellJob")
public class AddRainfallCellJob implements Job {
    @SneakyThrows
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Date date = new Date();
        date.setMinutes(0);
        date.setSeconds(0);
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date).replaceAll(" ","%20");
        HttpClientUtil.doGet("http://10.35.80.145:8888/test/addXlsx?time="+format);
    }
}
