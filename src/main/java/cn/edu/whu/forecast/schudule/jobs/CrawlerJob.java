package cn.edu.whu.forecast.schudule.jobs;

import cn.edu.whu.forecast.schudule.crawl.DownLoadPicFromUrlUtil;
import lombok.SneakyThrows;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class CrawlerJob implements Job {
    @SneakyThrows
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        HashMap<String,String> stations = new HashMap<>();
        stations.put("az9591","Z9591");
        stations.put("az9592","Z9592");
        stations.put("az9597","Z9597");
        stations.put("az9598","Z9598");
        stations.put("az9599","Z9599");
        for(String station:stations.keySet()){
            Calendar c1 = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            c1.set(Calendar.HOUR_OF_DAY, c1.get(Calendar.HOUR_OF_DAY) - 8);
            c1.set(Calendar.MINUTE,c1.get(Calendar.MINUTE)-18);
            c1.set(Calendar.SECOND, 0);
            c2.set(Calendar.MINUTE,c2.get(Calendar.MINUTE)-18);
            c2.set(Calendar.SECOND, 0);
            String UTCTime = new SimpleDateFormat("yyyyMMddHHmmss").format(c1.getTime());
            String StandardTime = new SimpleDateFormat("yyyyMMddHHmmss").format(c2.getTime());
            String StandardTimeWithoutHMS = new SimpleDateFormat("yyyyMMdd").format(c2.getTime());
            String url = "https://pi.weather.com.cn/i/product/pic/l/sevp_aoc_rdcp_sldas_ebref_"+station+"_l88_pi_"+UTCTime+"000.png";
            String root="/home/shanhong/app/python/quanzhou/example/radar/RADA_CHN_DOR_L3_MOCPUP_CR."+StandardTimeWithoutHMS+"-"+stations.get(station);
            String fileName = "Z_RADA_I_"+stations.get(station)+"_"+StandardTime+"_P_DOR_RDCP_CR.PNG";
            DownLoadPicFromUrlUtil.downloadPicture(url,root,fileName);
            //DownLoadPicFromUrlUtil.clearWaterMark(root,fileName);
        }
    }
}
