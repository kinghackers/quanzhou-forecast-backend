package cn.edu.whu.forecast.schudule.jobs;

import cn.edu.whu.forecast.util.DeleteFileUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

public class DelJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Date date=new Date();//取时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //把日期往后增加一天.整数往后推,负数往前移动(1:表示明天、-1：表示昨天，0：表示今天)
        calendar.add(Calendar.DATE,-3);

        //这个时间就是日期往后推一天的结果
        date=calendar.getTime();
        //String s = "RADA_CHN_DOR_L3_MOCPUP_CR.20220707-Z9591";
        String dateString = new SimpleDateFormat("yyyyMMdd").format(date);
        HashSet<String> set = new HashSet<>();
        set.add("Z9591");
        set.add("Z9592");
        set.add("Z9597");
        set.add("Z9598");
        set.add("Z9599");
        for(String station: set){
            DeleteFileUtil.deleteDirectory("/home/shanhong/app/python/quanzhou/example/radar/RADA_CHN_DOR_L3_MOCPUP_CR."+dateString+"-"+station);
        }
    }
}
