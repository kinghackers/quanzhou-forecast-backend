package cn.edu.whu.forecast.schudule.jobs;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallAllForecast;
import cn.edu.whu.forecast.service.impl.AvgRainfallAllServiceImpl;
import cn.edu.whu.forecast.util.ApplicationContextUtil;
import cn.edu.whu.forecast.util.HttpClientUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RadarJob implements Job {


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        AvgRainfallAllServiceImpl avgRainfallAllServiceImpl = (AvgRainfallAllServiceImpl) ApplicationContextUtil.getBean("AvgRainfallAllServiceImpl");


        String s = HttpClientUtil.doGet("http://124.220.210.6:8082/radar");
        JSONObject object = JSONObject.parseObject(s);
        JSONArray datas = object.getJSONArray("data");
        for(int i=0; i<datas.size(); i++) {
            JSONArray data = datas.getJSONArray(i);
            AvgRainfallAllForecast avgRainfallAllForecast = new AvgRainfallAllForecast();
            avgRainfallAllForecast.setTime(LocalDateTime.parse(data.getString(0).replaceAll("T", " "), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            avgRainfallAllForecast.setRainfallA(data.getBigDecimal(1));
            avgRainfallAllForecast.setRainfallB(data.getBigDecimal(2));
            avgRainfallAllForecast.setRainfallC(data.getBigDecimal(3));
            avgRainfallAllForecast.setRainfallD(data.getBigDecimal(4));
            avgRainfallAllForecast.setRainfallE(data.getBigDecimal(5));
            avgRainfallAllForecast.setRainfallF(data.getBigDecimal(6));
            avgRainfallAllForecast.setRainfallG(data.getBigDecimal(7));
            avgRainfallAllForecast.setRainfallH(data.getBigDecimal(8));
            avgRainfallAllForecast.setRainfallI(data.getBigDecimal(9));
            avgRainfallAllForecast.setRainfallJ(data.getBigDecimal(10));
            avgRainfallAllForecast.setRainfallYc(data.getBigDecimal(11));
            avgRainfallAllForecast.setEe(data.getBigDecimal(12));


            avgRainfallAllServiceImpl.addAvgRainfallAllForecast(avgRainfallAllForecast);
        }
    }
}
