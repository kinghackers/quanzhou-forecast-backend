package cn.edu.whu.forecast.service.impl;


import cn.edu.whu.forecast.common.PageBean;
import cn.edu.whu.forecast.entity.mysql.UserInfo;
import cn.edu.whu.forecast.mapper.mysql.UserInfoMapper;
import cn.edu.whu.forecast.service.UserService;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("UserServiceImpl")
public class UserServiceImpl implements UserService {
    @Resource
    private UserInfoMapper userInfoMapper;


    @Override
    public PageBean<UserInfo> getAllUser(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<UserInfo> allUser = userInfoMapper.getAllUser();

        return new PageBean<>(allUser);
    }

    @Override
    public List<UserInfo> getAllUser() {
        return userInfoMapper.getAllUser();
    }

    @Override
    public void addUser(String name, String username, String password,Boolean role) {
        UserInfo userInfo = new UserInfo();
        userInfo.setName(name);
        userInfo.setUsername(username);
        userInfo.setPassword(password);
        userInfo.setRole(role);
        userInfoMapper.insertSelective(userInfo);
    }

    @Override
    public void delUser(Integer id) {
        userInfoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateUser(UserInfo userInfo) {
        userInfoMapper.updateByPrimaryKeySelective(userInfo);
    }

    @Override
    public UserInfo getUser(Integer id) {
        return userInfoMapper.selectByPrimaryKey(id);
    }
}
