package cn.edu.whu.forecast.service.impl;

import cn.edu.whu.forecast.entity.mysql.RadarRecord;
import cn.edu.whu.forecast.mapper.mysql.RadarRecordMapper;
import cn.edu.whu.forecast.service.RadarService;
import cn.edu.whu.forecast.util.JsonFileUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class RadarServiceImpl implements RadarService {
    @Resource
    private RadarRecordMapper radarRecordMapper;


    @Override
    public List<RadarRecord> getAllRadarRecord() {
        return radarRecordMapper.getAllRadarRecord();
    }

    @Override
    public JSONObject getRadarRecordByTime(LocalDateTime time) {
        return JsonFileUtil.getDatafromFile(DateTimeFormatter.ofPattern("yyyyMMddHH").format(time));
    }

    @Override
    public int addRadarRecord(RadarRecord radarRecord) {
        return radarRecordMapper.insertSelective(radarRecord);
    }

    @Override
    public int deleteRadarRecord(LocalDateTime time) {
        return radarRecordMapper.deleteByPrimaryKey(time);
    }
}
