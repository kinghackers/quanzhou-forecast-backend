package cn.edu.whu.forecast.service;

import cn.edu.whu.forecast.common.PageBean;
import cn.edu.whu.forecast.vo.request.LogModifyVo;
import cn.edu.whu.forecast.vo.response.MessageVo;

import java.time.LocalDateTime;

public interface LogMessageService {
    PageBean<MessageVo> GetLogs(LocalDateTime startTime, LocalDateTime endTime, String type, int pageNum, int pageSize);

    int modifyStatus(LogModifyVo logModifyVo);

    void insertLog(Boolean isAuto, Integer type, String content);

    void insertLog(Boolean isAuto, Integer type, String content,String username);
}
