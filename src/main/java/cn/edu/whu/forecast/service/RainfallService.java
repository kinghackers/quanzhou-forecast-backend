package cn.edu.whu.forecast.service;

import cn.edu.whu.forecast.entity.oracle.STPPTNRALL;
import cn.edu.whu.forecast.vo.response.RainfallPerHourVo;
import cn.edu.whu.forecast.vo.response.RainfallRecordVo;
import cn.edu.whu.forecast.vo.response.RainfallVo;
import cn.edu.whu.forecast.vo.response.YcRainfallVo;


import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface RainfallService {
    List<YcRainfallVo> getYcRainfallByTime(LocalDateTime endTime);

    List<RainfallVo> getRainfallByTime(Date time);

    List<STPPTNRALL> getRainfallAll();

    //RainfallRecordVo getRainfallRecordByNow(Date endTime);

    public RainfallRecordVo getAvgRainfallByDistrictIdAndTime(Integer districtId,Date now);
}
