package cn.edu.whu.forecast.service;

import cn.edu.whu.forecast.common.PageBean;

import cn.edu.whu.forecast.entity.mysql.UserInfo;
import cn.edu.whu.forecast.mapper.mysql.UserInfoMapper;

import javax.annotation.Resource;
import java.util.List;

public interface UserService {

    PageBean<?> getAllUser(int pageNum, int pageSize);

    List<UserInfo> getAllUser();


    void addUser(String name, String username, String password,Boolean role);

    void delUser(Integer id);

    void updateUser(UserInfo userInfo);

    UserInfo getUser(Integer id);


}
