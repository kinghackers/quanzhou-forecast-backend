package cn.edu.whu.forecast.service.impl;

import cn.edu.whu.forecast.common.PageBean;
import cn.edu.whu.forecast.entity.mysql.WarningRecord;
import cn.edu.whu.forecast.mapper.mysql.WarningRecordMapper;
import cn.edu.whu.forecast.service.WarningRecordService;
import cn.edu.whu.forecast.vo.request.AddWarningRecordVo;
import cn.edu.whu.forecast.vo.response.RecordListVo;
import cn.edu.whu.forecast.vo.response.WarningRecordVo;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service("WarningRecordServiceImpl")
public class WarningRecordServiceImpl implements WarningRecordService {
    @Resource
    private WarningRecordMapper warningRecordMapper;

    @Override
    public PageBean<WarningRecordVo> getWarningRecord2(LocalDateTime startTime, LocalDateTime endTime, Integer warning, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<WarningRecord> warningRecords = warningRecordMapper.getWarningRecord2(startTime, endTime, warning);
        List<WarningRecordVo> vos = new ArrayList<>();
        warningRecords.forEach(warningRecord -> {
            WarningRecordVo warningRecordVo = new WarningRecordVo();
            BeanUtils.copyProperties(warningRecord,warningRecordVo);
            vos.add(warningRecordVo);
        });

        PageBean<WarningRecordVo> pageBean = new PageBean<>(vos);
        pageBean.setList(vos);
        return pageBean;
    }

    @Override
    public PageBean<WarningRecordVo> getWarningRecord1(LocalDateTime startTime, LocalDateTime endTime, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<WarningRecord> warningRecords = warningRecordMapper.getWarningRecord1(startTime, endTime);
        List<WarningRecordVo> vos = new ArrayList<>();
        warningRecords.forEach(warningRecord -> {
            WarningRecordVo warningRecordVo = new WarningRecordVo();
            BeanUtils.copyProperties(warningRecord,warningRecordVo);
            vos.add(warningRecordVo);
        });

        PageBean<WarningRecordVo> pageBean = new PageBean<>(vos);
        pageBean.setList(vos);
        return pageBean;
    }

    @Override
    public void addWarningRecord(AddWarningRecordVo addWarningRecordVo) {
        WarningRecord warningRecord = new WarningRecord();

        warningRecord.setCriticalRainfall(addWarningRecordVo.getCriticalRainfall());
        warningRecord.setWarning(addWarningRecordVo.getWarning());
        warningRecord.setHas_merge(addWarningRecordVo.getHas_merge());
        warningRecordMapper.insertSelective(warningRecord);
    }

    @Override
    public WarningRecord getWarningRecordByTime(LocalDateTime time) {
        return warningRecordMapper.getWarningRecordByTime(time);
    }

    @Override
    public List<RecordListVo> getWarningRecordTimeList() {
        List<WarningRecord> warningRecordTimeList = warningRecordMapper.getWarningRecordTimeList();
        List<RecordListVo> res = new ArrayList<>();
        for(WarningRecord warningRecord : warningRecordTimeList){
            RecordListVo recordListVo = new RecordListVo();
            recordListVo.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date.from(warningRecord.getCreatTime().atZone(ZoneId.systemDefault()).toInstant())));
            recordListVo.setHas_merge(warningRecord.getHas_merge());
            res.add(recordListVo);
        }

        return res;
    }
    @Override
    public void deleteWarningRecord(LocalDateTime time) {
        warningRecordMapper.deleteWarningRecord(time);
    }
}
