package cn.edu.whu.forecast.service.impl;

import cn.edu.whu.forecast.mapper.oracle.STPPTNRALLMapper;
import cn.edu.whu.forecast.mapper.oracle.STPPTNRMapper;
import cn.edu.whu.forecast.entity.mysql.RainfallStationInfo;
import cn.edu.whu.forecast.mapper.mysql.RainfallStationInfoMapper;
import cn.edu.whu.forecast.entity.oracle.STPPTNRALL;
import cn.edu.whu.forecast.service.RainfallService;
import cn.edu.whu.forecast.vo.response.RainfallPerHourVo;
import cn.edu.whu.forecast.vo.response.RainfallRecordVo;
import cn.edu.whu.forecast.vo.response.RainfallVo;
import cn.edu.whu.forecast.vo.response.YcRainfallVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Service("RainfallServiceImpl")
public class RainfallServiceImpl implements RainfallService {
    @Resource
    private STPPTNRMapper stpptnrMapper;

    @Resource
    private RainfallStationInfoMapper rainfallStationInfoMapper;

    @Resource
    private STPPTNRALLMapper stpptnrallMapper;


    @Override
    public List<YcRainfallVo> getYcRainfallByTime(LocalDateTime endTime) {
        return null;
    }

    @Override
    public List<RainfallVo> getRainfallByTime(Date time) {

        List<RainfallStationInfo> allRainfallStation = rainfallStationInfoMapper.getAllRainfallStation();
        List<STPPTNRALL> rainfallByTime = stpptnrallMapper.getRainfallByTime(time);
        List<RainfallVo> res = new ArrayList<>();
        for(RainfallStationInfo stationInfo:allRainfallStation){
            for (STPPTNRALL stpptnrall:rainfallByTime){
                if(stationInfo.getStcd().equals(stpptnrall.getStcd())){
                    RainfallVo rainfallVo = new RainfallVo();
                    rainfallVo.setTime(stpptnrall.getTm());
                    rainfallVo.setLatitude(stationInfo.getLatitude());
                    rainfallVo.setLongitude(stationInfo.getLatitude());
                    if (stpptnrall.getDrp()==null) rainfallVo.setRainfall(new BigDecimal(0));
                    else rainfallVo.setRainfall(stpptnrall.getDrp());
                    rainfallVo.setStationName(stationInfo.getName());
                    res.add(rainfallVo);
                }
            }
        }


        return res;
    }

    @Override
    public List<STPPTNRALL> getRainfallAll() {
        //List<RainfallStationInfo> allRainfallStation = rainfallStationInfoMapper.getAllRainfallStation();

        return stpptnrallMapper.getRainfallAll();
    }


    //之前的需求是只要永春站的实测降雨，现在需要的是分区降雨、、详见getAvgRainfallByDistrictIdAndTime()
//    @Override
//    public RainfallRecordVo getRainfallRecordByNow(Date endTime) {
//        //todo 如果需要新增站点选择,则去掉这个STNM，参数增加一个String的STNM
//        final String STNM = "永春站";
//        Date startTime = new Date(endTime.getTime()-1000*60*60*9);
//
//        List<STPPTNRALL> rainfallRecordByNow = stpptnrallMapper.getRainfallRecordByNow(startTime, endTime);
//
//        RainfallRecordVo rainfallRecordVo = new RainfallRecordVo();
//        List<RainfallPerHourVo> list = new ArrayList<>();
//        for(STPPTNRALL stpptnrall:rainfallRecordByNow){
//            RainfallPerHourVo rainfallPerHourVo = new RainfallPerHourVo();
//            rainfallPerHourVo.setTime(new SimpleDateFormat("HH:mm").format(stpptnrall.getTm()));
//            if(stpptnrall.getDrp()==null){
//                rainfallPerHourVo.setRainfall(new BigDecimal(0));
//            }else{
//                rainfallPerHourVo.setRainfall(stpptnrall.getDrp());
//            }
//            list.add(rainfallPerHourVo);
//        }
//        rainfallRecordVo.setStationName(STNM);
//        rainfallRecordVo.setDataList(list);
//
//        return rainfallRecordVo;
//    }

    public RainfallRecordVo getAvgRainfallByDistrictIdAndTime(Integer districtId,Date endTime) {
        Date startTime = new Date(endTime.getTime()-1000*60*60*9);
        HashMap<Integer, Double[]> weightMap = new HashMap<>();
        weightMap.put(1,new Double[]{0.7,0.09,0.22,0.0,0.0,0.0,0.0});
        weightMap.put(2,new Double[]{0.0,0.54,0.38,0.0,0.08,0.0,0.0});
        weightMap.put(3,new Double[]{0.18,0.0,0.58,0.23,0.02,0.0,0.0});
        weightMap.put(4,new Double[]{0.15,0.0,0.0,0.85,0.0,0.0,0.0});
        weightMap.put(5,new Double[]{0.0,0.0,0.01,0.92,0.07,0.0,0.0});
        weightMap.put(6,new Double[]{0.0,0.0,0.0,1.0,0.0,0.0,0.0});
        weightMap.put(7,new Double[]{0.0,0.0,0.0,0.05,0.88,0.0,0.07});
        weightMap.put(8,new Double[]{0.0,0.0,0.0,0.6,0.22,0.0,0.17});
        weightMap.put(9,new Double[]{0.0,0.1,0.0,0.0,0.21,0.62,0.07});
        weightMap.put(10,new Double[]{0.0,0.0,0.0,0.0,0.0,0.0,1.0});
        HashMap<Integer,String> districtNameMap = new HashMap<>();
        districtNameMap.put(1,"A");
        districtNameMap.put(2,"B");
        districtNameMap.put(3,"C");
        districtNameMap.put(4,"D");
        districtNameMap.put(5,"E");
        districtNameMap.put(6,"F");
        districtNameMap.put(7,"G");
        districtNameMap.put(8,"H");
        districtNameMap.put(9,"I");
        districtNameMap.put(10,"J");

        Double[] weight = weightMap.get(districtId);
        List<STPPTNRALL> JX = stpptnrallMapper.getRainfallRecordByNow(startTime, endTime,"71340100");
        List<STPPTNRALL> SS = stpptnrallMapper.getRainfallRecordByNow(startTime, endTime,"71341200");
        List<STPPTNRALL> PH = stpptnrallMapper.getRainfallRecordByNow(startTime, endTime,"71342306");
        List<STPPTNRALL> DZ = stpptnrallMapper.getRainfallRecordByNow(startTime, endTime,"71343206");
        List<STPPTNRALL> DQ = stpptnrallMapper.getRainfallRecordByNow(startTime, endTime,"71343300");
        List<STPPTNRALL> TM = stpptnrallMapper.getRainfallRecordByNow(startTime, endTime,"71344200");
        List<STPPTNRALL> QC = stpptnrallMapper.getRainfallRecordByNow(startTime, endTime,"71344306");

        List<List<STPPTNRALL>> list = new ArrayList<>();
        list.add(JX);
        list.add(SS);
        list.add(PH);
        list.add(DZ);
        list.add(DQ);
        list.add(TM);
        list.add(QC);
        while(JX.size()<10){
            JX.add(new STPPTNRALL());
        }
        while(SS.size()<10){
            SS.add(new STPPTNRALL());
        }
        while(PH.size()<10){
            PH.add(new STPPTNRALL());
        }
        while(DZ.size()<10){
            DZ.add(new STPPTNRALL());
        }
        while(DQ.size()<10){
            DQ.add(new STPPTNRALL());
        }
        while(TM.size()<10){
            TM.add(new STPPTNRALL());
        }
        while(QC.size()<10){
            QC.add(new STPPTNRALL());
        }

        List<RainfallPerHourVo> AVG = new ArrayList<>();
        for(int i=0; i<10; i++){
            RainfallPerHourVo rainfallPerHourVo = new RainfallPerHourVo();
            BigDecimal tmp = new BigDecimal(0);
            for(int j=0; j<weight.length; j++) {
                tmp = tmp.add(list.get(j).get(i).getDrp() == null ? (list.get(j).get(i).getGpsDrp()==null?new BigDecimal(0):list.get(j).get(i).getGpsDrp()) : list.get(j).get(i).getDrp());
                tmp = tmp.multiply(BigDecimal.valueOf(weight[j]));

            }
            tmp = tmp.setScale(2,BigDecimal.ROUND_HALF_UP);
            rainfallPerHourVo.setTime((new SimpleDateFormat("yyyy-MM-dd HH:mm").format(endTime.getTime()-1000*60*60*(9-i))));
            rainfallPerHourVo.setRainfall(tmp);
            AVG.add(rainfallPerHourVo);
        }
        RainfallRecordVo rainfallRecordVo = new RainfallRecordVo();
        rainfallRecordVo.setDistrictName(districtNameMap.get(districtId));
        rainfallRecordVo.setDataList(AVG);
        return rainfallRecordVo;
    }
}
