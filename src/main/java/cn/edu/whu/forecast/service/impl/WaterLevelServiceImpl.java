package cn.edu.whu.forecast.service.impl;

import cn.edu.whu.forecast.entity.oracle.STRIVERR;
import cn.edu.whu.forecast.mapper.oracle.STRIVERRMapper;
import cn.edu.whu.forecast.service.WaterLevelService;
import cn.edu.whu.forecast.util.CSVFileUtil;
import cn.edu.whu.forecast.util.HttpClientUtil;
import cn.edu.whu.forecast.vo.response.FlowPerHourVo;
import cn.edu.whu.forecast.vo.response.FlowVo;
import cn.edu.whu.forecast.vo.response.WaterLevelPerHourVo;
import cn.edu.whu.forecast.vo.response.WaterLevelVo;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class WaterLevelServiceImpl implements WaterLevelService {
    @Resource
    private STRIVERRMapper striverrMapper;

    @Override
    public WaterLevelVo getWaterLevelRecordByNow(Date endTime) {
        //todo 跟降雨过程的获取一样，如果后续要加选择站点，则需要添加String的STCD参数
        final String STNM = "永春";
        Date startTime = new Date(endTime.getTime()-1000*60*60*9);
        List<STRIVERR> waterLevelVoByNow = striverrMapper.getWaterLevelVoByNow(startTime, endTime);
        List<WaterLevelPerHourVo> list = new ArrayList<>();
        WaterLevelVo waterLevelVo = new WaterLevelVo();
        for(STRIVERR striverr: waterLevelVoByNow){
            WaterLevelPerHourVo waterLevelPerHourVo = new WaterLevelPerHourVo();
            waterLevelPerHourVo.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(startTime));
            waterLevelPerHourVo.setWaterLevel(striverr.getZ());
            list.add(waterLevelPerHourVo);
            startTime = new Date(startTime.getTime()+1000*60*60);
        }
        waterLevelVo.setStationName(STNM);
        waterLevelVo.setDataList(list);

        return waterLevelVo;
    }

    @Override
    public FlowVo getFlowRecordByNow(Date endTime) {

        List<List<Object>> heads = new ArrayList<>();
        List<Object> tmpHead = new ArrayList<>();
        tmpHead.add("TIME");
        tmpHead.add("YC_Z");
        heads.add(tmpHead);
        List<List<Object>> dataList=  new ArrayList<>();
        List<Object> data = new ArrayList<>();
        data.add("TIME");
        data.add("YC_Z");
        dataList.add(data);
        //写永春水位.csv
        final String STNM = "永春";
        Date startTime = new Date(endTime.getTime()-1000*60*60*9);
        List<STRIVERR> waterLevelVoByNow = striverrMapper.getWaterLevelVoByNow(startTime, endTime);
        int diff = 10-waterLevelVoByNow.size();
        int tmpdiff = diff;
        while(tmpdiff>0){
            waterLevelVoByNow.add(new STRIVERR());
            tmpdiff--;
        }
        Date date = new Date();
        date.setSeconds(0);
        date.setMinutes(0);

        for(int i=0; i<waterLevelVoByNow.size(); i++){
            STRIVERR striverr = waterLevelVoByNow.get(i);
            List<Object> tmp = new ArrayList<>();
            tmp.add(new SimpleDateFormat("yyyy/MM/dd HH:mm").format(striverr.getTm()==null?(date.getTime()-1000*60*60*(diff--)):striverr.getTm()));
            tmp.add(striverr.getZ()==null?waterLevelVoByNow.get(i-1).getZ():striverr.getZ());
            dataList.add(tmp);
        }

        String fileName = "永春水位.csv";
        String filePath = "/home/shanhong/app/python/quanzhou/";

        CSVFileUtil.createCSV(heads,dataList,fileName,filePath,false);



        //调用水位转流量函数
        String s = HttpClientUtil.doGet("http://10.35.80.145:8082/zq");


        //获取流量数据

        JSONObject object = JSONObject.parseObject(s);
        JSONArray timelist = object.getJSONArray("timelist");
        JSONArray data_flow = object.getJSONArray("data");

        List<FlowPerHourVo> list = new ArrayList<>();

        for(int i=0; i<data_flow.size(); i++){
            JSONArray dataFlowJSONArray = data_flow.getJSONArray(i);
            FlowPerHourVo flowPerHourVo = new FlowPerHourVo();
            flowPerHourVo.setTime(timelist.getString(i).substring(11));
            BigDecimal bigDecimal = dataFlowJSONArray.getBigDecimal(0);
            flowPerHourVo.setWaterLevel(bigDecimal.setScale(2,BigDecimal.ROUND_HALF_UP));
            list.add(flowPerHourVo);
        }

        FlowVo flowVo = new FlowVo();
        flowVo.setStationName(STNM);
        flowVo.setDataList(list);

        return flowVo;
    }
}
