package cn.edu.whu.forecast.service.impl;

import cn.edu.whu.forecast.cache.CacheEntity;
import cn.edu.whu.forecast.cache.CacheManagerImpl;
import cn.edu.whu.forecast.entity.mysql.AvgRainfallAllForecast;
import cn.edu.whu.forecast.entity.mysql.PreAvgRainfallAll;
import cn.edu.whu.forecast.mapper.mysql.AvgRainfallAllForecastMapper;
import cn.edu.whu.forecast.mapper.mysql.PreAvgRainfallAllMapper;
import cn.edu.whu.forecast.service.AvgRainfallAllService;
import cn.edu.whu.forecast.util.HttpClientUtil;
import cn.edu.whu.forecast.vo.response.RainfallForecastVo;
import cn.edu.whu.forecast.vo.response.RainfallPerHourVo;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mysql.cj.xdevapi.JsonArray;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("AvgRainfallAllServiceImpl")
public class AvgRainfallAllServiceImpl implements AvgRainfallAllService {
    @Resource
    AvgRainfallAllForecastMapper avgRainfallAllForecastMapper;

    @Resource
    PreAvgRainfallAllMapper preAvgRainfallAllMapper;

    @Resource
    CacheManagerImpl cacheManager;

    @Override
    public void addAvgRainfallAllForecast(AvgRainfallAllForecast avgRainfallAllForecast) {
        avgRainfallAllForecastMapper.insert(avgRainfallAllForecast);
    }

    @Override
    public void addPreAvgRainfallAll(PreAvgRainfallAll preAvgRainfallAll) {
        preAvgRainfallAllMapper.insert(preAvgRainfallAll);
    }

    @Override
    public Object getAvgRainfallAllForecast() {
        //这里原本要传Date
        //todo 后续如果需要新增选择区域，则新增一个Integer的分区Id参数
        //todo 这里也有时间问题 差了九小时
//        final String DISTRICTNAME = "永春";
//
//
//        String s = HttpClientUtil.doGet("http://10.35.80.145:8082/radar");
//        JSONObject object = JSONObject.parseObject(s);
//        JSONArray datas = object.getJSONArray("data");
//
//        List<RainfallPerHourVo> list = new ArrayList<>();
//        for(int i=0; i<datas.size(); i++){
//            JSONArray data = datas.getJSONArray(i);
//
//            RainfallPerHourVo rainfallPerHourVo = new RainfallPerHourVo();
//            rainfallPerHourVo.setTime(data.getString(0).substring(11,16));
//            rainfallPerHourVo.setRainfall(data.getBigDecimal(11));
//            list.add(rainfallPerHourVo);
//        }
//        RainfallForecastVo rainfallForecastVo = new RainfallForecastVo();
//        rainfallForecastVo.setDistrictName(DISTRICTNAME);
//        rainfallForecastVo.setDataList(list);
//        rainfallForecastVo.setRadar(object.getJSONArray("radar"));
//
//        return rainfallForecastVo;

        CacheEntity radar = cacheManager.getCacheByKey("radar");
        return radar.getData();

    }
}
