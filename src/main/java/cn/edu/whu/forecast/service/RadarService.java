package cn.edu.whu.forecast.service;


import cn.edu.whu.forecast.entity.mysql.RadarRecord;
import com.alibaba.fastjson.JSONObject;

import java.time.LocalDateTime;
import java.util.List;

public interface RadarService {
    List<RadarRecord> getAllRadarRecord();

    JSONObject getRadarRecordByTime(LocalDateTime time);

    int addRadarRecord(RadarRecord radarRecord);

    int deleteRadarRecord(LocalDateTime time);
}
