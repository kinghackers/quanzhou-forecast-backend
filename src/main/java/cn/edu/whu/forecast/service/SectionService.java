package cn.edu.whu.forecast.service;

import cn.edu.whu.forecast.entity.mysql.SectionFlowWaterLevel;
import cn.edu.whu.forecast.vo.response.DistrictWaterLevelAndFlowForecastVo;

import java.time.LocalDateTime;
import java.util.List;

public interface SectionService {
    void addFlowWaterLevel(SectionFlowWaterLevel sectionFlowWaterLevel);

    DistrictWaterLevelAndFlowForecastVo getWaterLevelAndFlowForecastByTimeAndDistrict(LocalDateTime startTime, Integer districtId);
}
