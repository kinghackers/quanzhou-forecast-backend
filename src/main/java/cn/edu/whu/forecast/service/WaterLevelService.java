package cn.edu.whu.forecast.service;

import cn.edu.whu.forecast.vo.response.FlowVo;
import cn.edu.whu.forecast.vo.response.WaterLevelVo;

import java.util.Date;
import java.util.List;

public interface WaterLevelService {
    WaterLevelVo getWaterLevelRecordByNow(Date endTime);

    FlowVo getFlowRecordByNow(Date endTime);
}
