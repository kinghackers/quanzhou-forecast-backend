package cn.edu.whu.forecast.service;

import cn.edu.whu.forecast.common.PageBean;
import cn.edu.whu.forecast.entity.mysql.WarningRecord;
import cn.edu.whu.forecast.vo.request.AddWarningRecordVo;
import cn.edu.whu.forecast.vo.response.WarningRecordVo;

import java.time.LocalDateTime;
import java.util.List;

public interface WarningRecordService {
    PageBean<WarningRecordVo> getWarningRecord2(LocalDateTime startTime, LocalDateTime endTime, Integer warning, int pageNum, int pageSize);
    PageBean<WarningRecordVo> getWarningRecord1(LocalDateTime startTime, LocalDateTime endTime, int pageNum, int pageSize);

    void addWarningRecord(AddWarningRecordVo addWarningRecordVo);

    WarningRecord getWarningRecordByTime(LocalDateTime time);

    List<?> getWarningRecordTimeList();

    void deleteWarningRecord(LocalDateTime time);
}
