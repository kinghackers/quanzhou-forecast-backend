package cn.edu.whu.forecast.service.impl;

import cn.edu.whu.forecast.common.PageBean;
import cn.edu.whu.forecast.entity.mysql.HydrologyStationInfo;
import cn.edu.whu.forecast.entity.mysql.RainfallStationInfo;
import cn.edu.whu.forecast.entity.oracle.STPPTNRALL;
import cn.edu.whu.forecast.mapper.mysql.HydrologyStationInfoMapper;
import cn.edu.whu.forecast.mapper.mysql.RainfallStationInfoMapper;
import cn.edu.whu.forecast.mapper.oracle.STPPTNRALLMapper;
import cn.edu.whu.forecast.service.BasicInformationService;
import cn.edu.whu.forecast.vo.response.RainfallPerHourVo;
import cn.edu.whu.forecast.vo.response.StationInfoVo;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class BasicInformationImpl implements BasicInformationService {
    @Resource
    private RainfallStationInfoMapper rainfallStationInfoMapper;

    @Resource
    private HydrologyStationInfoMapper hydrologyStationInfoMapper;


    @Override
    public PageBean<StationInfoVo> getRainfallStation(int pageNum, int pageSize) {

        PageHelper.startPage(pageNum,pageSize);
        List<RainfallStationInfo> allRainfallStations = rainfallStationInfoMapper.getAllRainfallStation();
        List<StationInfoVo> vos = new ArrayList<>();
        allRainfallStations.forEach(rainfallStationInfo -> {
            StationInfoVo stationInfoVo = new StationInfoVo();
            BeanUtils.copyProperties(rainfallStationInfo,stationInfoVo);
            vos.add(stationInfoVo);
        });

        PageBean<StationInfoVo> pageBean = new PageBean<>(vos);
        pageBean.setList(vos);
        return pageBean;
    }

    @Override
    public PageBean<StationInfoVo> getHydrologyStation(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<HydrologyStationInfo> hydrologyStations = hydrologyStationInfoMapper.getAllHydrologyStation();
        List<StationInfoVo> vos = new ArrayList<>();
        hydrologyStations.forEach(hydrologyStation -> {
            StationInfoVo stationInfoVo = new StationInfoVo();
            BeanUtils.copyProperties(hydrologyStation,stationInfoVo);
            vos.add(stationInfoVo);
        });

        PageBean<StationInfoVo> pageBean = new PageBean<>(vos);
        pageBean.setList(vos);
        return pageBean;
    }


}
