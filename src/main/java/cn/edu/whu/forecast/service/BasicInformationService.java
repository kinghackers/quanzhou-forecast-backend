package cn.edu.whu.forecast.service;

import cn.edu.whu.forecast.common.PageBean;
import cn.edu.whu.forecast.vo.response.RainfallPerHourVo;
import cn.edu.whu.forecast.vo.response.StationInfoVo;
import cn.edu.whu.forecast.vo.response.WaterLevelVo;

import java.util.Date;
import java.util.List;

/**
* 基本信息查询
* @author fullangleblank
* @since 2022/6/27
*/
public interface BasicInformationService {

    PageBean<StationInfoVo> getRainfallStation(int pageNum, int pageSize);

    PageBean<StationInfoVo> getHydrologyStation(int pageNum, int pageSize);



    //void addStation(AddStationVo addStationVo);

    //List<CriticalLevelVo> getCriticalLevelList();



}
