package cn.edu.whu.forecast.service;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallAllForecast;
import cn.edu.whu.forecast.entity.mysql.PreAvgRainfallAll;
import cn.edu.whu.forecast.vo.response.RainfallForecastVo;

import java.util.Date;
import java.util.List;

public interface AvgRainfallAllService {
    void addAvgRainfallAllForecast(AvgRainfallAllForecast avgRainfallAllForecast);

    void addPreAvgRainfallAll(PreAvgRainfallAll preAvgRainfallAll);

    Object getAvgRainfallAllForecast();
}
