package cn.edu.whu.forecast.common;

import cn.edu.whu.forecast.enums.ResultCode;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 自定义Api响应结构
 * @author fullangleblank
 * @since 2022/6/8
 */
@Data
public class ResultBean<T> {
    @ApiModelProperty(value = "响应状态码，200表示成功")
    private int code = ResultCode.OK.getCode();

    /**
     * 响应消息
     * */
    @ApiModelProperty(value = "响应状态信息")
    private String msg = ResultCode.OK.getMsg();
    /**
     * 响应中的数据
     * */
    @ApiModelProperty(value = "响应数据")
    private T data;

    private ResultBean() {

    }

    private ResultBean(ResultCode resultCode) {
        this.code = resultCode.getCode();;
        this.msg = resultCode.getMsg();
    }

    private ResultBean(T data) {
        this.data = data;
    }



    /**
     * 业务处理成功,无数据返回
     * */
    public static ResultBean<Void> ok() {
        return new ResultBean<>();
    }

    /**
     * 业务处理成功，有数据返回
     * */
    public static <T> ResultBean<T> ok(T data) {
        return new ResultBean<>(data);
    }

    /**
     * 业务处理失败
     * */
    public static ResultBean<Void> fail(ResultCode resultCode) {
        return new ResultBean<>(resultCode);
    }


    /**
     * 系统错误
     * */
    public static ResultBean<Void> error() {
        return new ResultBean<>(ResultCode.ERROR);
    }
}
