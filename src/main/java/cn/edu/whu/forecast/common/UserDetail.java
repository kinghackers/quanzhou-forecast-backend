package cn.edu.whu.forecast.common;

import lombok.Data;
/**
* 登录用户封装
* @author fullangleblank
* @since 2022/6/15
*/
@Data
public class UserDetail {
    private Long id;
    private String username;
}
