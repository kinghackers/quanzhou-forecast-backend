package cn.edu.whu.forecast.mapper.oracle;

import cn.edu.whu.forecast.entity.oracle.STRIVERR;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface STRIVERRMapper {
    int deleteByPrimaryKey(@Param("stcd") String stcd, @Param("tm") Date tm, @Param("type") String type);

    int insert(STRIVERR record);

    int insertSelective(STRIVERR record);

    STRIVERR selectByPrimaryKey(@Param("stcd") String stcd, @Param("tm") Date tm, @Param("type") String type);

    int updateByPrimaryKeySelective(STRIVERR record);

    int updateByPrimaryKey(STRIVERR record);

    List<STRIVERR> getWaterLevelVoByNow(@Param("date1") Date startTime,@Param("date2") Date endTime);
}