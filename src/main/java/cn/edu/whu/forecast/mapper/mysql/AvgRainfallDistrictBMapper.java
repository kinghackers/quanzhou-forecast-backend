package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallDistrictB;
import java.time.LocalDateTime;

public interface AvgRainfallDistrictBMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallDistrictB record);

    int insertSelective(AvgRainfallDistrictB record);

    AvgRainfallDistrictB selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallDistrictB record);

    int updateByPrimaryKey(AvgRainfallDistrictB record);
}