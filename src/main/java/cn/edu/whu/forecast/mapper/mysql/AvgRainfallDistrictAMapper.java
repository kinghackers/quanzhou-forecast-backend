package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallDistrictA;
import java.time.LocalDateTime;

public interface AvgRainfallDistrictAMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallDistrictA record);

    int insertSelective(AvgRainfallDistrictA record);

    AvgRainfallDistrictA selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallDistrictA record);

    int updateByPrimaryKey(AvgRainfallDistrictA record);
}