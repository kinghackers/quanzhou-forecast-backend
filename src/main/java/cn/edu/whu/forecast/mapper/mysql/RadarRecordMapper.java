package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.RadarRecord;
import java.time.LocalDateTime;
import java.util.List;

public interface RadarRecordMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(RadarRecord record);

    int insertSelective(RadarRecord record);

    List<RadarRecord> getAllRadarRecord();

    RadarRecord getRadarRecordByTime(LocalDateTime time);
}