package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.SectionInfo;
import org.apache.ibatis.annotations.Param;

public interface SectionInfoMapper {
    int deleteByPrimaryKey(@Param("id") Integer id, @Param("districtId") Integer districtId);

    int insert(SectionInfo record);

    int insertSelective(SectionInfo record);

    SectionInfo selectByPrimaryKey(@Param("id") Integer id, @Param("districtId") Integer districtId);

    int updateByPrimaryKeySelective(SectionInfo record);

    int updateByPrimaryKey(SectionInfo record);
}