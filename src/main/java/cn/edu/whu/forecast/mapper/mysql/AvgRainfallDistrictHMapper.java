package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallDistrictH;
import java.time.LocalDateTime;

public interface AvgRainfallDistrictHMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallDistrictH record);

    int insertSelective(AvgRainfallDistrictH record);

    AvgRainfallDistrictH selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallDistrictH record);

    int updateByPrimaryKey(AvgRainfallDistrictH record);
}