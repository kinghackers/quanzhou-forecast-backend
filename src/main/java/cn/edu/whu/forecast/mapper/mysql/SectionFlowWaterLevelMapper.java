package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.SectionFlowWaterLevel;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface SectionFlowWaterLevelMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SectionFlowWaterLevel record);

    int insertSelective(SectionFlowWaterLevel record);

    SectionFlowWaterLevel selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SectionFlowWaterLevel record);

    int updateByPrimaryKey(SectionFlowWaterLevel record);

    List<SectionFlowWaterLevel> getWaterLevelAndFlowForecastByTimeAndDistrict(@Param("startTime")LocalDateTime startTime, @Param("endTime")LocalDateTime endTime, @Param("id") Integer sectionId);
}