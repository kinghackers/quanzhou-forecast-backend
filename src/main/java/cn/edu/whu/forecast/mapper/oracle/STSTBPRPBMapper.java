package cn.edu.whu.forecast.mapper.oracle;

import cn.edu.whu.forecast.entity.oracle.STSTBPRPB;

public interface STSTBPRPBMapper {
    int deleteByPrimaryKey(String stcd);

    int insert(STSTBPRPB record);

    int insertSelective(STSTBPRPB record);

    STSTBPRPB selectByPrimaryKey(String stcd);

    int updateByPrimaryKeySelective(STSTBPRPB record);

    int updateByPrimaryKey(STSTBPRPB record);
}