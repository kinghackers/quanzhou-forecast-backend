package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallDistrictJ;
import java.time.LocalDateTime;

public interface AvgRainfallDistrictJMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallDistrictJ record);

    int insertSelective(AvgRainfallDistrictJ record);

    AvgRainfallDistrictJ selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallDistrictJ record);

    int updateByPrimaryKey(AvgRainfallDistrictJ record);
}