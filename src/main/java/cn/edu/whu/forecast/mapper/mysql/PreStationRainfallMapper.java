package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.PreStationRainfall;

public interface PreStationRainfallMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PreStationRainfall record);

    int insertSelective(PreStationRainfall record);

    PreStationRainfall selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PreStationRainfall record);

    int updateByPrimaryKey(PreStationRainfall record);
}