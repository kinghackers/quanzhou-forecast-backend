package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.DistrictInfo;

public interface DistrictInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DistrictInfo record);

    int insertSelective(DistrictInfo record);

    DistrictInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DistrictInfo record);

    int updateByPrimaryKey(DistrictInfo record);
}