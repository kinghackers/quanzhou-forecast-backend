package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallAllForecast;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface AvgRainfallAllForecastMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallAllForecast record);

    int insertSelective(AvgRainfallAllForecast record);

    AvgRainfallAllForecast selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallAllForecast record);

    int updateByPrimaryKey(AvgRainfallAllForecast record);

    List<AvgRainfallAllForecast> getAvgRainfallAllForecast(@Param("date1") Date now, @Param("date2") Date end);
}