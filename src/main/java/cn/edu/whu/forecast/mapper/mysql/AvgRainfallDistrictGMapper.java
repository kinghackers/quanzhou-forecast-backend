package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallDistrictG;
import java.time.LocalDateTime;

public interface AvgRainfallDistrictGMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallDistrictG record);

    int insertSelective(AvgRainfallDistrictG record);

    AvgRainfallDistrictG selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallDistrictG record);

    int updateByPrimaryKey(AvgRainfallDistrictG record);
}