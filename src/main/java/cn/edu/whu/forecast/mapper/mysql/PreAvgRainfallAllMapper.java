package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.PreAvgRainfallAll;

import java.time.LocalDateTime;

public interface PreAvgRainfallAllMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(PreAvgRainfallAll record);

    int insertSelective(PreAvgRainfallAll record);

    PreAvgRainfallAll selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(PreAvgRainfallAll record);

    int updateByPrimaryKey(PreAvgRainfallAll record);
}