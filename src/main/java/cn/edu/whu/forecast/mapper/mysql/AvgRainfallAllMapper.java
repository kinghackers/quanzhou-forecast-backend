package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallAll;
import java.time.LocalDateTime;

public interface AvgRainfallAllMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallAll record);

    int insertSelective(AvgRainfallAll record);

    AvgRainfallAll selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallAll record);

    int updateByPrimaryKey(AvgRainfallAll record);
}