package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallDistrictYC;
import java.time.LocalDateTime;

public interface AvgRainfallDistrictYCMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallDistrictYC record);

    int insertSelective(AvgRainfallDistrictYC record);

    AvgRainfallDistrictYC selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallDistrictYC record);

    int updateByPrimaryKey(AvgRainfallDistrictYC record);
}