package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.HydroAvgRainfallAll;
import java.time.LocalDateTime;

public interface HydroAvgRainfallAllMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(HydroAvgRainfallAll record);

    int insertSelective(HydroAvgRainfallAll record);

    HydroAvgRainfallAll selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(HydroAvgRainfallAll record);

    int updateByPrimaryKey(HydroAvgRainfallAll record);
}