package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallDistrictF;
import java.time.LocalDateTime;

public interface AvgRainfallDistrictFMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallDistrictF record);

    int insertSelective(AvgRainfallDistrictF record);

    AvgRainfallDistrictF selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallDistrictF record);

    int updateByPrimaryKey(AvgRainfallDistrictF record);
}