package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.RainfallStationInfo;

import java.util.List;

public interface RainfallStationInfoMapper {
    int deleteByPrimaryKey(String stcd);

    int insert(RainfallStationInfo record);

    int insertSelective(RainfallStationInfo record);

    RainfallStationInfo selectByPrimaryKey(String stcd);

    int updateByPrimaryKeySelective(RainfallStationInfo record);

    int updateByPrimaryKey(RainfallStationInfo record);

    List<RainfallStationInfo> getAllRainfallStation();
}