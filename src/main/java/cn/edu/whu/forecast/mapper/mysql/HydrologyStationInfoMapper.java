package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.HydrologyStationInfo;

import java.util.List;

public interface HydrologyStationInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HydrologyStationInfo record);

    int insertSelective(HydrologyStationInfo record);

    HydrologyStationInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HydrologyStationInfo record);

    int updateByPrimaryKey(HydrologyStationInfo record);

    List<HydrologyStationInfo> getAllHydrologyStation();
}