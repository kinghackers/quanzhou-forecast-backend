package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.WarningRecord;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface WarningRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(WarningRecord record);

    int insertSelective(WarningRecord record);

    WarningRecord selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(WarningRecord record);

    int updateByPrimaryKey(WarningRecord record);

    WarningRecord getWarningRecordByTime(@Param("time") LocalDateTime time);


    List<WarningRecord> getWarningRecord2(@Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime, @Param("warning") Integer warning);
    List<WarningRecord> getWarningRecord1(@Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);

    List<WarningRecord> getWarningRecordTimeList();

    void deleteWarningRecord(@Param("time") LocalDateTime time);
}