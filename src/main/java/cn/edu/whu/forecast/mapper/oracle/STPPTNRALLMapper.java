package cn.edu.whu.forecast.mapper.oracle;

import cn.edu.whu.forecast.entity.oracle.STPPTNRALL;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface STPPTNRALLMapper {
    int deleteByPrimaryKey(@Param("stcd") String stcd, @Param("tm") Date tm, @Param("type") String type);

    int insert(STPPTNRALL record);

    int insertSelective(STPPTNRALL record);

    STPPTNRALL selectByPrimaryKey(@Param("stcd") String stcd, @Param("tm") Date tm, @Param("type") String type);

    int updateByPrimaryKeySelective(STPPTNRALL record);

    int updateByPrimaryKey(STPPTNRALL record);

    List<STPPTNRALL> getRainfallByTime(@Param("time") Date time);

    List<STPPTNRALL> getRainfallAll();

    List<STPPTNRALL> getRainfallRecordByNow(@Param("date1") Date startTime, @Param("date2") Date endTime,@Param("STCD") String STCD);
}