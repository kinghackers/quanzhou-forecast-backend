package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallDistrictI;
import java.time.LocalDateTime;

public interface AvgRainfallDistrictIMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallDistrictI record);

    int insertSelective(AvgRainfallDistrictI record);

    AvgRainfallDistrictI selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallDistrictI record);

    int updateByPrimaryKey(AvgRainfallDistrictI record);
}