package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallDistrictC;
import java.time.LocalDateTime;

public interface AvgRainfallDistrictCMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallDistrictC record);

    int insertSelective(AvgRainfallDistrictC record);

    AvgRainfallDistrictC selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallDistrictC record);

    int updateByPrimaryKey(AvgRainfallDistrictC record);
}