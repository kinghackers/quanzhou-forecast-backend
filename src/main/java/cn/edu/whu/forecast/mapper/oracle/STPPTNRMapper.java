package cn.edu.whu.forecast.mapper.oracle;

import cn.edu.whu.forecast.entity.oracle.STPPTNR;
import java.util.Date;
import org.apache.ibatis.annotations.Param;

public interface STPPTNRMapper {
    int deleteByPrimaryKey(@Param("stcd") String stcd, @Param("tm") Date tm, @Param("type") String type);

    int insert(STPPTNR record);

    int insertSelective(STPPTNR record);

    STPPTNR selectByPrimaryKey(@Param("stcd") String stcd, @Param("tm") Date tm, @Param("type") String type);

    int updateByPrimaryKeySelective(STPPTNR record);

    int updateByPrimaryKey(STPPTNR record);
}