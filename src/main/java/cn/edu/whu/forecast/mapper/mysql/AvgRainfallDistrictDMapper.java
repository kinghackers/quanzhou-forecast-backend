package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallDistrictD;
import java.time.LocalDateTime;

public interface AvgRainfallDistrictDMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallDistrictD record);

    int insertSelective(AvgRainfallDistrictD record);

    AvgRainfallDistrictD selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallDistrictD record);

    int updateByPrimaryKey(AvgRainfallDistrictD record);
}