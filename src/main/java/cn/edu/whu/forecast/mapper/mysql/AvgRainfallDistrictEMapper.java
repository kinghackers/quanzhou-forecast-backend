package cn.edu.whu.forecast.mapper.mysql;

import cn.edu.whu.forecast.entity.mysql.AvgRainfallDistrictE;
import java.time.LocalDateTime;

public interface AvgRainfallDistrictEMapper {
    int deleteByPrimaryKey(LocalDateTime time);

    int insert(AvgRainfallDistrictE record);

    int insertSelective(AvgRainfallDistrictE record);

    AvgRainfallDistrictE selectByPrimaryKey(LocalDateTime time);

    int updateByPrimaryKeySelective(AvgRainfallDistrictE record);

    int updateByPrimaryKey(AvgRainfallDistrictE record);
}