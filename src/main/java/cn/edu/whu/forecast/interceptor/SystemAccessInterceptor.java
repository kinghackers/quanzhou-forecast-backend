package cn.edu.whu.forecast.interceptor;

import cn.edu.whu.forecast.util.JwtUtil;
//import cn.edu.whu.forecast.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.util.Objects;
@Component
public class SystemAccessInterceptor implements HandlerInterceptor {
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        response.setCharacterEncoding("UTF-8");
//        response.setContentType("application/json");
//
//        String token = request.getHeader("token");
//        if(StringUtils.isEmpty(token)||JwtUtil.isExpiration(token)){
//            HttpServletResponseWrapper httpResponse = new HttpServletResponseWrapper((HttpServletResponse) response);
//            System.out.println(request.getRequestURI());
//            String path=request.getRequestURI();
//            if(path.contains("/forecast")){
//                path = path.replaceAll("/forecast/test","/other/testOther");
//                request.getRequestDispatcher(path).forward(request,response);
//            }
//        }

        //UserInfo userDetails = (UserInfo) redisTemplate.opsForValue().get(token);

        //return !Objects.isNull(userDetails);

        return true;
    }


}
