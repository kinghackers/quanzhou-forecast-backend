# quanzhou-forecast-backend

#### Description
基于天气雷达短临降雨预报和数值降雨预报数据和山洪模拟技术，构建中小河流域水文模型与水动力模型相结合的山洪模拟预报模型；研究建立中小河流域洪水风险动态评估技术，确定山洪预警指标阈值，并在实时洪水风险评估技术基础上，建立山区中小河流流域山洪预警方法；基于B/S模式，采用SOA架构，以WEBGIS为基础平台，开发基于智能渐进决策机制的中小河流域洪水预报预警模块，并嵌入泉州市现有防汛系统中。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
