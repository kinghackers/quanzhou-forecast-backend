# quanzhou-forecast-backend

#### 介绍
基于天气雷达短临降雨预报和数值降雨预报数据和山洪模拟技术，构建中小河流域水文模型与水动力模型相结合的山洪模拟预报模型；研究建立中小河流域洪水风险动态评估技术，确定山洪预警指标阈值，并在实时洪水风险评估技术基础上，建立山区中小河流流域山洪预警方法；基于B/S模式，采用SOA架构，以WEBGIS为基础平台，开发基于智能渐进决策机制的中小河流域洪水预报预警模块，并嵌入泉州市现有防汛系统中。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
